{
  description = "Flake d'infrastructure BIB";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    # Dépot de configuration privée
    services.url = "git+ssh://git@framagit.org/bib/inf/srv";
    nixVirt = {
      url = "https://flakehub.com/f/AshleyYakeley/NixVirt/*.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs:
  {
    nixosConfigurations = {
      alambix = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = with inputs; [
          nixVirt.nixosModules.default
          services.nixosModules.alambix
          ./alambix/configuration.nix
          ./alambix/hardware-configuration.nix
        ];
      };

      bibix = inputs.nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = with inputs; [
          services.nixosModules.bibix
          ./bibix/configuration.nix
          ./bibix/hardware-configuration.nix
        ];
      };
    };
  };
}

