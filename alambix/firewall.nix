{ config, lib, pkgs, ... }:

{
  environment.etc.ethertypes.source = "${pkgs.iptables}/etc/ethertypes";

}
