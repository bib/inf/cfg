{ config, lib, pkgs, ... }:

let
  ssh_keys = {
    pistache = [ 
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDAP7dxz43XD7OVFHLRIvp7QLNk3QD8VCuWkOYsrfFLp3gX9mD5/YIGp0pfAUG2eZjwcA1ef0fCERChwUqO4Wrz0xaQ1PD7hlC+6XqDCwCDNEGnufOZbilQxlpuroZUQ/TEVhbHYxTK2hyV63eVvDh56dI3IqeoxqU1oY5BnXZjHJYkWPbkIxhsgGTlW5nJfkgZyKDie1O79NJdzQmqVuZt5z6yKhnuX2+gOgRC5hfto8tWLZj9Hy9d15VKL1b0VfuCK+mpANRhQds7YsfMfTTYMIDfuFCI8ZpYWQxNw7t8QXbfLaMsiRgphGZEr555g+xdkXCZ2+Oj4rlPfKgMzXLQ2gFv0Aen4wwzEdjhXdiwuKbbbsI4WKd3kvX5Nc70wKYxvI8fQs0GlO9FLdEbC8R6J15JKREmJlgizImmqb8KIME4bGsYGOVPFVbRHQKjUT1rFVWD3+aDml80cAlHOpfoln7C4oqCi+oDDmAuyH358O2S4EdIGSGe4I2i0eJvxn3EA8ZVLKUWj/lcyJ05eFHdLZWcLyC4op8faNGqhdgG0I/tRJ/sFgpCj0Z7b5afMKxdX/LHYtOqcpxoQpT/mwIEQe6VPpUHQC7xrOB/kKIc5DNSVZdIQnaqRYvUrgPrWhXR9MG3/7Gt4lq0j39n1+Fm7aj6L9CmLHc3vfwCttv/9Q== openpgp:0x451F2F0F" 
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDMGtqHweV6S1xUCWfxcFv1QgJ2XkisStY+y5fSdvRP4Abz91ZKyghXwlvme7sVCQ1wDvjBAaeHSy214aXYhvHhW75SnAKB4bSxsNgizKCWYzScmbuCBOWwL6aYX2661e+wxtC/UUIb7r1HAYclQ/xAXN6C1f0b2eg3bmWGgVKPCZdAFHBuCKwhtfxpOWJA3YzaEreP4oGn0Xh/cMgMQRKt0fvvPK6S0xUFI2aMufa7YVZF+983XOQ7PMwrpu5Yr1b6fO+0Vz31iFYkeQqlRpQIhKbxxhwF58C2Ert2Yh0at6bu0cn1uRDh3WgOKxb6ayXxRulPO1U2pnqiWrORuNy7cKzxaBh/noSCmn52iWNw1w/1bw9XcSFdVkRNkoNeWgC7+naWn5qweB69KoBGeKnaPoYE+l96LdMNs7bDEnn6NV/NjvMH5A1c+HBBQWOKfaQ8qNlqowhW/YATK3HXqO5alJYDx4cj1kz8azOWIh6gS7ofhyJ7ZLpqKhxA4DOrQr8= user@debian"
    ];
    smyds = [
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCoerbEBysWo+cJ6SfWTJuCadhJcwGvPdsGF4dmzJrK/X5v8impivie1s7iS8dfYUsUgiAolVqaKAa1cE+E5J5VHRv+bMbpbUjeLR3YCtBtCnr4Zp9YOwsgPH7QN6O7kuZTv9RaoXbDs/OVM1UnYIzFVwoBiiEokqBgCwy0RYo400raFJ+xRk4x/qyUsYLn1jxo3h/z3IK1V2BuaXEa2g7n1/CG6wSgIZ5z6TiCWp0sRFLWpfmqAoWML71SdLl+/opy57zrPO0bUtCKFzupTo3MzyQdaFavm5OXuId5y1qm4zi/X6fHTljwU3igkvmvsjjCvxTbamgouxyhEl5b2HnX smyds@riseup.net"
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDM9uMwDKZblXo049iVNYbxAe23HsYHDVaD34l5wJh2c+c9mWTQiwz9+o1x4z/th1v4Zxql8prmcOCLRGUUftu/aYfWKxY3aEFahHYDmR77k/EwD/EH8HIzvRYB7duPNIRH99NBVv4uN3sbkKFCo8f8oj05G8Sca0MfqOWaclbc5/sTXbjS5K13GIAjEhSu0fgceKT1I8FsPG4uiBzLQE8H0NPkDnRPrRjfdYHlLtR3pTa4qNfUybfW77RBheM6BYCaVx7uZ2mFzVSuRKmErBL/nUVR0AHKKPEYzlX97yJSSrsmG7glMM4e1nS/xenOg/+9IpC6MO9ViS4SjMmn1tlEcDCd+lPcq9Acv06EZ9FwubfHf4ns+iBfbIhwXW9lNKOUdk4UzkFuYK3t438u2C0XZQRM4lEIMjXYZ97e2oN9YxE0yqAtpWdo38ASRozfG7anRlj7eT/hPbSnaipG0NcOLWMVT0i1OIWbKtqNvT6wb4hMmsQinZA6CNt8n2+4FQE= openpgp:0x1653E51A"
    ];
    wargreen = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBRUVHxWsppYlotxeFzTA8HHROSDW/P6nKjAeD9aqUR+QVhiK88mBmTAYjjPbWrxXR214O3u45I5RykyNks5llfYndGcb0tuapZU8T/ELQBHNfAeVl6FxBx4iDYfZ3B3DpSkDk5Rg8HO09qfUAI5S5SUcSIkDQxEufASqBA2cY4KQMgMISz0K4Dy3JelibGBXCCzW+e2ggECyoUfXseQc9Q7CkRH8r2F2xYPQQRA4LWsfcENNzqL/9n737l+QWWLeaJdBx4L+zfYXxNxnciSHctHxXSBUp6WE6yTsA6UzrwfrlDJvmALEQtOa7hlFcc/l9055kmAjRswEe3XUh8rYv wargreen@LaChoze"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXDiGOe86c6RdKktNScQ3VVfhl/dPqJMd9FNtrhgZDLJ3A+0P4E97HVWrahWtYMumsYv133ZdOT1rBNJ61XwqVBNAa22DRg3k0w0ADsMVkOZYr7RbOqbVPT1uL/AFMbo6pDYTYCkVC+/0G2zclOHELaYJg0ZwO1wyMhfmtDWTwxfADiSsb1oq6ve+ybrwc/FMq0iyDRw5zP/8lJGSEGDJTmwbzo54NlMiAVrW+Kp5iVcGk7FGjak3wAiShZTfTiGATDNW/ET1gHTl2m8/QxM04bb5GzTPcXKU+1No4SW3ptAhHPAAB+Y9NgGkZXLpr4euYZDQnz2x+DsTPgxOYbUCZ4OQSBtBVVTAaVj88fLh9/oqmBpD3SSbhjS52q+6sDozDzfMwwjPZ3fHul3ispKbrZVTS6oRbRCyhLCAe6yGH9E+pi1xIaiYzhZQxXGGJos4w9F8BdNxWSAPLsC/3GlSQTcMjAGKa/6fntJ3ZKRXPFU4mcxZEka2kWOlosCDFcTM= wargreen@fetide"
    ];
    martial = [
      # "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCfquN30HesLdZUXxVTEvElqVTFL4iPha+6JeVwlhbzJDgiZZcuEcrsjQl4ZqsjYC5CTg8SkKZelds4Za5PZ/BGziJMspQuv9SbQWHjIj8Znh+85yZBs2l3uxfdHDroEQEAci0PWi7/RsCqP+qJhj4emO2TWKU1yhfI6zk4AEcn9TXI+8cXSqZxvTkjbr3ldEtMGmiIaIkUI53DE2sHc1eq1bkZCrXHqEZkrWYZ6r+hW5qB0RPsZGTEhIY2Jl+OdK6wb11ILQ94cjqhaPvOYBWG5SnBcgLOSpGmdYlsFvCate2paIWSA61KxAbRIzdq7cfsjrSXlw8VZVZlcYyB/oAXAb8XWyvP25al2X+Oyu7JAGAKNNqccVGLmjlcqIFvVOuH9VqN+6tFdy1+aemlzf07hiTic1VcSvVoLMLZBubIpj1tzhv9afs20gdptN4G3PSZJ9skiVPqWXxO1WNqWZkGscsYQSSzGoVKUHClj2K+wrB99g4FSHFzIBl+HAMUi80= osef"
    ];
  };
in

{
  security.sudo.wheelNeedsPassword = false;

  users.motd = ''
    +---------------+--------------------------------------------------------------+
    |  NixOS 24.05  |  alambix.lebib.org, bare-metal host                          |
    |   \\  \\ //   |    Admins      : smyds, pistache, wargreen                   |
    |  ==\\__\\/ // |    Services    : KVM host, WireGuard, BIRD, haproxy          |
    |    //   \\//  |    Router-ID   : #10.13.255.200                              |
    | ==//     //== |    CPU         : 2 Intel(R) Xeon(R) 4114 CPU (10) @ 2.20GHz  |
    |  //\\___//    |    Memory      : 100874 MiB                                  |
    | // /\\  \\==  |    Storage     : 2 SSDs                                      |
    |   // \\  \\   |                                                              |
    +---------------+--------------------------------------------------------------+
  '';

  users.users.pistache = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.pistache;
  };

  users.users.smyds = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.smyds;
  };

#  users.users.benbart = {
#    isNormalUser = true; extraGroups = [ "wheel" ];
#    openssh.authorizedKeys.keys = ssh_keys.benbart;
#  };

  users.users.wargreen = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.wargreen;
  };

  users.users.martial = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.martial;
  };

  users.users.root.openssh.authorizedKeys.keys = lib.concatLists (lib.attrValues ssh_keys);
  boot.initrd.network.ssh.authorizedKeys = lib.concatLists (lib.attrValues ssh_keys);

}
