{ config, lib, pkgs, ... }: 
{
  imports = [
    (import (builtins.fetchGit {
      url = https://framagit.org/bib/dev/nix/irq-affinity.git;
      rev = "1d635141e727bc518ff1ceee45debed4883d492e";
    }))
  ];

#  boot.loader.grub.enable = true;
  # NOTE: remember to add other devices in the mirror, and sync their /boot
  boot.loader.timeout = 5;
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 3;

  boot.kernelPackages = pkgs.linuxPackages_hardened;

  # configure Linux kernel command line
  boot.kernelParams = [
#    # enable passthrough for integrated graphics and audio
#    "vfio-pci.ids=8086:5912,8086:a121,8086:a170,8086:a123"
#
#    # disable framebuffers to avoid initializing integrated graphics
#    "video=efifb:off,vesafb:off" "nomodeset"

    # enable the IOMMU
    "intel_iommu=on" "iommu=on"

    # limit the ZFS ARC cache size
    "zfs.zfs_arc_max=1073741824"

    # secondary console on TTY, primary console on serial (COM1)
    # "console=tty0" "console=ttyS0"

    # page poisoning
    # "page_poison=on" "slub_debug=P"
  ];

  boot.extraModprobeConfig = ''
    options kvm-intel nested=Y
  '';
  boot.kernelModules = [
    "pcspkr"                              # beeper (XXX: does not work)
    "vfio-pci"                            # VFIO passthrough
    "nvme"                                # NVMe storage
  ];

  boot.blacklistedKernelModules = [
  ];

  boot.kernel.sysctl = {
    "vm.overcommit_memory" = "1";
    "net.ipv4.ip_forward" = "1";
  };

  boot.irqAffinity = {
  };

  # Make ZFS request encryption credentials using systemd-askpass
  boot.zfs.requestEncryptionCredentials = true;

  # Configure the initramfs so that the disks can be unlocked remotely
  boot.initrd = {
    
    # Use systemd in stage1
    systemd.enable = true;

    # Use the same network configuration as in stage2
    systemd.network = config.systemd.network;

    # Load the network drivers
    availableKernelModules = [ "e1000e" "igb" ];

    # Enable an SSH server that runs /bin/systemd-query-passwords when root logs in
    network.enable = true;
    network.ssh.enable = true;
    network.ssh.port = 23;
    network.ssh.hostKeys = [
      "/etc/keys/ssh-initrd/ssh_host_rsa_key"
      "/etc/keys/ssh-initrd/ssh_host_ed25519_key"
    ];

    # systemd.storePaths = [ unlockScript ];
    network.ssh.extraConfig = ''
      Match User root
        ForceCommand ${pkgs.systemd}/bin/systemd-tty-ask-password-agent --watch
    '';

    # Provide some useful utilites in the initrd
    systemd.initrdBin = with pkgs; [
      iproute2 iputils iptables nftables
      tcpdump netcat gnugrep moreutils
    ];
  };
}
