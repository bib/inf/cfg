{ config, pkgs, lib, ... }:
{

  networking = {
    hostName = "alambix";
    hostId = "1acbee12";
    useDHCP = false;
    wireguard.enable = true;
    firewall.enable = false;
    nameservers = [ "127.0.0.1" ];
    iproute2.enable = true;
    iproute2.rttablesExtraConfig = ''
      22400 internal
    '';
  };

  systemd.network = {
    enable = true;
    netdevs = {
      br-eko.netdevConfig = {
        Kind = "bridge";
        Name = "br-eko";
      };
    };
    networks = {
      # Loopback interface
      loopback = {
        name = "lo";
        address = [ "127.0.0.1/8" "10.13.255.203/32" ];
      };

      # Uplink bridge (@cogent)
      eno3 = {
        name = "eno3";
        addresses = [
          { Address = "89.234.152.36/27"; }
          { Address = "2a00:5881:a150:36::/64"; }
        ];
        gateway = [ "89.234.152.33" ];
      };

      # Uplink bridge (@bib)
      eno4 = {
        name = "eno4";
        networkConfig.DHCP = "ipv4";
        linkConfig.RequiredForOnline = false;
      };

      # Uplink tunnel to `roko.pstch.net`
      tun-uplink = {
        name = "tun-uplink";
        addresses = [
          { Address = "10.13.255.203/32"; Peer = "10.14.255.255/32"; }
        ];
        routes = [
          {  Destination = "10.14.0.0/16" ; Scope = "link"; }
          {  Destination = "10.14.255.240/32" ; Scope = "link"; }
        ];
        linkConfig.RequiredForOnline = false;
      };

      # Downlink bridge to `eko.lebib.org`
      br-eko = {
        name = "br-eko";
        addresses = [
          { Address = "10.13.255.203/32"; Peer = "10.13.255.223/32"; }
        ];
        linkConfig.RequiredForOnline = false;
        networkConfig.IPMasquerade = true;
      };
    };
  };
}
