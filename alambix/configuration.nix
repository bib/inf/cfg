{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./dns-recursor.nix
      ./firewall.nix
      ./hardware-configuration.nix
      ./kernel.nix
      ./networking.nix  
      ./system.nix  
      ./users.nix  
      ./virtualization.nix
    ];
}


