{ config, pkgs, lib, ... }:

with lib; with builtins; with debug;
with strings; with attrsets; with generators;

# script exit codes:
#  - 65 :: EX_DATAERR : Unexpected image hash
#  - 69 :: EX_UNAVAILABLE : Missing implicit object
#  - 78 :: EX_CONFIG : Non-implicit no-overwrite object already exists
#  - 76 :: EX_PROTOCOL : LXD client error
#  - 71 :: EX_OSERR : Container runtime rrror

let
  # Definitions
  # ===========================================================================

  cfg = config.virtualisation.lxd;
  lxdBinDir = "${cfg.package or pkgs.lxd}/bin";
  commonLxdArgs = "--quiet --force-local";
  tmpDir = "/tmp/nixos-lxd";
  lockDir = "${tmpDir}/image-locks";
  dumpDir = "${tmpDir}/config-dumps";
  nixosRootPath = "/run/current-system/sw";
  makeNixosConfig = import <nixpkgs/nixos>;
  makeNixosTarball = pkgs.path + "/nixos/lib/make-system-tarball.nix";
  lxdGuestModule = import (builtins.fetchGit {
    url = https://framagit.org/bib/dev/nix/lxd-guest.git;
    rev = "832b7c57f16acdcb979ddd4b334ca6f1a8e1fe34";
  });

  makeAttrs = {name, value}: { "${name}" = value; };
  defaultString = value: default: if value == null then default else value;
  nonEmpty = x: x != "";
  nonEmptyList = sep: xs: lib.concatStringsSep sep (filter nonEmpty xs);
  commaList = nonEmptyList ", ";
  lineList = nonEmptyList "\n";

  storageUnitName = s: "lxd-storage-${s}";
  projectUnitName = p: "lxd-project-${p}";
  imageUnitName = i: "lxd-image-${i}";
  profileUnitName = p: n: "lxd-profile-${p}-${n}";
  containerUnitName = p: c: "lxd-container-${p}-${c}";
  getBaseImageName = p: n: "${p}-${n}-base";
  hasOwnImage = cont: ! (isString cont.image);
  getImage = project: name: cont: (
    if hasOwnImage cont
    then { name = getBaseImageName project name; value = cont.image; }
    else { name = cont.image; value = getAttr cont.image cfg.images; }
  );

  serverConfig = lxdConfig "lxd-server" cfg.config;
  inlineContainers = project: filterAttrs (name: hasOwnImage) project.containers;
  inlineImages = name: project: mapAttrs' (getImage name) (inlineContainers project);
  projectImageList = name: project: attrValues (inlineImages name project);
  inlineImageList = concatLists (mapAttrsToList projectImageList cfg.projects);
  imageList = inlineImageList ++ (attrValues cfg.images);
  anyImage = cond: any cond imageList;

  # Templates
  # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  hyperTemplateConfig = ''
    templates:
      /etc/nixos/hypervisor-configuration.nix
        when:
          - create
          - rename
          - copy
        template:
          hypervisor-configuration.tpl
  '';
  hyperConfigTemplate = pkgs.writeText "hypervisor-configuration.tpl" ''
    { config, pkgs, lib, ... }:
    {
      networking.hostname = mkDefault "{{ container['name'] }}";
    }
  '';
  lxdInitialConfig = ''
    { config, pkgs, lib, ... }:
    {
      imports = [
        ./lxd-guest                     # (provided by the image)
        ./hypervisor-configuration.nix  # (written by the hypervisor)
      ];

      # ...
    }
  '';

  # LXD configuration
  # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  lxdContainerConfig = image: container: fold recursiveUpdate {} [
    ({
      inherit (container) profiles;
      config."raw.lxc" = ''
        lxc.apparmor.profile=${getApparmorProfile container image}
      '';
    })
    (optionalAttrs (container.idmapBase != null) {
      config."security.idmap.base" = container.idmapBase;
    })
    (optionalAttrs (container.idmapSize != null) {
      config."security.idmap.size" = container.idmapSize;
    })
    (optionalAttrs container.isolated {
      config."security.idmap.isolated" = true;
    })
    (optionalAttrs image.nixosShareStore { devices."nix-store" = {
      type = "disk"; readonly = true;
      source = "/nix/store"; path = "/nix/store";
    };})

    (optionalAttrs image.nixosShareDaemon { devices."nix-daemon" = {
      type = "disk";
      source = "/nix/var/nix/daemon-socket"; path = "/nix/var/nix/daemon-socket";
    };})
    container.config
  ];
  lxdConfig = name: config: pkgs.writeText "${name}.yaml" (toYAML {} config);

  # AppArmor
  # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  defaultApparmorAbstractions = {
    base = ''
      deny mount fstype=devpts,
      mount options=(rw, rshared) -> /,
      mount options=(rw, make-rslave) -> /,
      mount options=(rw, rbind) /boot/ -> /boot/,
      mount options=(rw, rbind) /bin/ -> /bin/,
      mount options=(rw, rbind) /etc/ -> /etc/,
      mount options=(rw, nosuid, remount, strictatime) -> /dev/,
      mount options=(rw, nosuid, noexec, remount) -> /dev/pts/,
      mount options=(rw, nosuid, nodev, noexec, remount) -> /proc/,
    '';
    cgns = ''
      mount fstype=cgroup -> /sys/fs/cgroup/**,
      mount fstype=cgroup2 -> /sys/fs/cgroup/**,
    '';
    systemd = ''
      mount options=(rw, rbind) / -> /run/systemd/unit-root/,
      mount options=(rw, rbind) /run/systemd/inacessible/dir/ -> /home/,
      mount options=(rw, rbind) /** -> /run/systemd/unit-root/**,
      mount options=(rw, nosuid, remount, bind) -> /run/systemd/unit-root/**,
    '';
    nixos = ''
      mount fstype=ramfs options=(rw, nosuid, nodev) -> /run/keys/,
      mount options=(ro, remount, bind, relatime) -> /nix/store/,
      mount options=(rw, remount, bind, relatime) -> /nix/store/,
    '';
  };
  defaultApparmorProfiles = {
    lxd-default = ''
      #include <abstractions/lxc/container-base>
      #include <abstractions/nixos-lxd/base>
      #include <abstractions/nixos-lxd/cgns>
      #include <abstractions/nixos-lxd/systemd>
    '';
    lxd-nixos = ''
      #include <abstractions/lxc/container-base>
      #include <abstractions/nixos-lxd/base>
      #include <abstractions/nixos-lxd/cgns>
      #include <abstractions/nixos-lxd/systemd>
      #include <abstractions/nixos-lxd/nixos>
    '';
    };
  getApparmorProfile = container: image: defaultString container.apparmorProfile (
    getAttr image.type {
      lxd = "lxd-default";
      tarball = "lxd-default";
      nixos = "lxd-nixos";
    }
  );

  apparmorAbstraction = name: text: {
    name = "etc/apparmor.d/abstractions/nixos-lxd/${name}";
    path = pkgs.writeText "apparmor-abstractions-lxd-${name}" text;
  };
  apparmorProfile = name: text: pkgs.writeText "apparmor-profile-lxd-${name}" ''
    #include <tunables/global>
    profile ${name} flags=(attach_disconnected,mediate_deleted) {
      #include <abstractions/lxc/container-base>
      ${text}
    }
  '';
  apparmorAbstractions = mapAttrsToList apparmorAbstraction cfg.apparmor.abstractions;
  apparmorPackage = pkgs.linkFarm "lxd-container-abstractions" apparmorAbstractions;
  apparmorProfiles = mapAttrsToList apparmorProfile cfg.apparmor.profiles;

  # Tarballs
  # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  nixosMetaTarball = image: let
    description = commaList [
      "NixOS"
      (optionalString image.nixosPersistent "persistent")
      (optionalString image.nixosShareStore "shared Nix store")
      (optionalString image.nixosShareDaemon "shared Nix daemon")
    ] + optionalString (image.description != "") "(${image.description})";
    metadata = pkgs.writeText "metadata.yaml" ''
      architecture: ${removeSuffix "-linux" currentSystem}
      creation_date: 1
      properties:
        description: ${description}
        os: NixOS
        release: ${config.system.nixos.release}
      ${optionalString image.nixosPersistent hyperTemplateConfig}
    '';
  in pkgs.callPackage makeNixosTarball { contents = [ {
    target = "/metadata.yaml"; source = metadata; }
  ] ++ ( optional image.nixosPersistent {
    target = "/templates/hyper-config.nix"; source = hyperConfigTemplate;
  } ); } + "/tarball/*";

  nixosRootTarball = image: let
    pkgs2storeContents = map (x: { object = x; symlink = "none"; });
    source = image.nixosConfig { inherit config pkgs lib; };
    configuration = recursiveUpdate source {
      imports = source.imports or [] ++ [ lxdGuestModule ];
      config.nixpkgs.pkgs = lib.mkOverride 0 pkgs;
      config.environment.noXlibs = mkDefault true;
      config.documentation.enable = false;
      config.documentation.nixos.enable = false;
    };
    result = makeNixosConfig { inherit configuration; };
    config = result.config;
  in pkgs.callPackage makeNixosTarball {
    contents = (optionals image.nixosPersistent [
      { target = "/etc/nixos/lxd-guest"; source = lxdGuestModule; }
      { target = "/etc/nixos/configuration.nix"; source = lxdInitialConfig; }
    ]) ++ (optionals image.nixosShareStore [
      { target = "/sbin/init"; source = config.system.build.toplevel + "/init"; }
    ]);
    storeContents = optionals (! image.nixosShareStore) ( [ {
      object = config.system.build.toplevel + "/init"; symlink = "/sbin/init";
    } ] ++ pkgs2storeContents [ pkgs.stdenv ]);
    extraCommands = "mkdir -p proc sys dev"; extraArgs = "--owner=0";
    compressCommand = "${pkgs.coreutils}/bin/cat"; compressionExtension = "";
  } + "/tarball/*";
  nixosImageHash = image: pkgs.runCommand "image-hash" {} ''
    cat ${nixosMetaTarball image} ${nixosRootTarball image} \
      | sha256sum | sed "s/  -//" > $out
  '';

  # NOTE: for the nixos type, this should be precomputed
  makeImageHash = image: getAttr image.type {
    tarball = "cat ${image.tarballMetadata} ${image.tarballRootfs} | sha256sum | sed 's/  -//'";
    nixos = "cat ${nixosImageHash image}";
    lxd = "$lxc image info ${image.lxdRemote}:${image.lxdImage} | grep -Po '^Fingerprint: \\K.*$'";
  };

  # Image scripts
  # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  imageStartScript = name: image: let
    importArgs = metadata: rootfs: "import ${metadata} ${rootfs}";
    copyArgs = remote: image: "copy ${remote}:${image} local:";
    createArgs = getAttr image.type {
      tarball = importArgs  image.tarballMetadata image.tarballRootfs;
      nixos = importArgs (nixosMetaTarball image) (nixosRootTarball image);
      lxd = copyArgs image.lxdRemote image.lxdImage;
    };
  in ''
    # Cleanup old images and aliases
    lockfile=${lockDir}/$active.lock
    mkdir -p ${lockDir}; touch $lockfile
    ( ${pkgs.utillinux}/bin/flock -e 200
      if $lxc image show ${name} >/dev/null 2>&1; then
        active=$($lxc image list ${name} --format csv -cF)
        echo "active image hash: '$active'"
        if [ "$active" != "$target" ]; then
          echo "image hash changed, need rebuild"
          echo "removing image alias..."
          $lxc image alias delete ${name}
          if ! ( $lxc image alias list $active | grep -q "" ); then
            echo "removing active image..."
            $lxc image delete $active
          else
            echo "keeping image because of other aliases"
          fi
        fi
      fi
    ) 200> $lockfile
    # Create new aliases and images
    lockfile=${lockDir}/$target.lock
    touch $lockfile
    ( ${pkgs.utillinux}/bin/flock -e 201
      if ! $lxc image show ${name} >/dev/null 2>&1; then
        if $lxc image show $target >/dev/null 2>&1; then
          echo "found target image, adding alias..."
          $lxc image alias create ${name} $target
        else
          echo "importing image..."
          $lxc image ${createArgs} --alias ${name}
          active=$($lxc image list ${name} --format csv -cF)
          if [ "$active" != "$target" ]; then
            echo "ERROR: Image was created with an unexpected hash."
            echo "   expected: $target"
            echo "    current: $active"
            exit 65
          fi
        fi
      fi
    ) 201> $lockfile
  '';

  imageStopScript = name: image: optionalString (! image.cached) ''
    # Cleanup aliases and images
    lockfile=${lockDir}/$active.lock
    mkdir -p ${lockDir}; touch $lockfile
    ( ${pkgs.utillinux}/bin/flock -e 200
      if $lxc image show ${name} >/dev/null 2>&1; then
        active=$($lxc image list ${name} --format csv -cF)
        echo "active image hash: $active"
        echo "removing image alias..."
        $lxc image alias delete ${name}
        if ! ( $lxc image alias list $active --format csv | grep -q "" ); then
          echo "removing active image..."
          $lxc image delete $active
        else
          echo "keeping image because of other aliases"
        fi
      fi
    ) 200> ${lockDir}/$active.lock
  '';

  # systemd service template
  # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  lxdService = {
    name, unitName, description, type, target,
    createScript, deleteScript,
    extraArgs ? "", commonScript ? ":",
    setupScript ? ":", cleanupScript ? ":",
    startScript ? ":", stopScript ? ":",
    unitConfig ? {}, serviceConfig ? {}, dependencies ? [],
    persistent ? false, implicit ? false,
    overwrite ? false, immutable ? false,
  }: { name = unitName; value = let
    configCommand = if type == "container" then "config" else type;
    dumpFile = "${dumpDir}/${unitName}.yaml";
    setOwner = "$lxc ${configCommand} set ${name} user.system true";
    ownerCheck = "$lxc ${configCommand} get ${name} user.system | grep -q '^true$'";
    existCheck = "$lxc ${configCommand} show ${name} >/dev/null 2>&1";
    reuseScript = ''echo "reusing existing ${type}, as we created it"'';
    missingScript = ''
      echo "ERROR: missing implicit ${type}";
      exit 69
    '';
    replaceScript = if overwrite then ''
      echo "overwriting existing ${type}"
    '' else if implicit then ''
      echo "using implicit ${type}"
    '' else ''
      echo "ERROR: a ${type} with the same name already exists "
      exit 78
    '';
    safeDeleteScript = ''
      if ${ownerCheck}; then
        ${deleteScript}
      fi
    '';
    safeCreateScript = ''
      set -x
      if ${existCheck}; then
        mkdir -p ${dumpDir}
        # save volatile settings
        $lxc ${configCommand} show ${name} > ${dumpFile};
        owner=$($lxc ${configCommand} get ${name} user.system)
        # run setup script
        ${setupScript}
        $lxc ${configCommand} set ${name} user.system "$owner"
        # restore volatile settings
        ${pkgs.yq}/bin/yq -r '.config |
          with_entries( select(.key | startswith("volatile"))) |
          to_entries|map("\(.key) \(.value|tostring)")|.[]' ${dumpFile} | \
          while read value; do $lxc ${configCommand} set ${name} $value; done
        rm -f ${dumpFile};
      else
        ${createScript}
        ${setOwner}
      fi
      set +x
    '';
  in {
    inherit description unitConfig;
    serviceConfig = {
      User = "lxd";
      Type = "oneshot";
      RemainAfterExit = "yes";
    } // serviceConfig;
    stopIfChanged = true;
    after = [ "lxd.service" ] ++ dependencies;
    requires = [ "lxd.service" ] ++ dependencies;
    requiredBy = [ target ];
    partOf = [ target ];
    script = ''
      lxc="${lxdBinDir}/lxc ${commonLxdArgs} ${extraArgs}"
      ${commonScript}
      if ${existCheck}; then
        if ${ownerCheck}; then
          ${reuseScript}
        else
          ${replaceScript}
        fi
        ${optionalString implicit cleanupScript}
      else
        ${if implicit then missingScript else ":"}
      fi
      ${optionalString (! implicit) safeCreateScript}
      ${startScript}
    '';
    preStop = ''
      lxc="${lxdBinDir}/lxc ${commonLxdArgs} ${extraArgs}"
      ${commonScript}
      if ${existCheck}; then
        ${stopScript}
        ${optionalString (! persistent) safeDeleteScript}
      fi
    '';
  }; };

  # system services
  # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  storageService = name: storage: let
    unitName = storageUnitName name;
    makePair = key: value: ''${key}="${value}"'';
    configPairs = mapAttrsToList makePair storage.config;
    storageConfig = concatStringsSep " " configPairs;
  in lxdService  {
    inherit name unitName;
    inherit (storage) persistent overwrite implicit;
    type = "storage";
    description = "LXD storage ${name}";
    target = "${projectUnitName name}.target";
    createScript = ''
      echo "creating backend storage..."
      $lxc storage create ${name} ${storage.driver} ${storageConfig}
    '';
    deleteScript = ''
      echo "attempting to delete storage backend..."
      $lxc storage delete ${name} || true
    '';
  };

  imageService = name: image: {
    name = imageUnitName name;
    value = {
      description = "LXD image ${name}";
      serviceConfig = {
        User = "lxd";
        Type = "oneshot";
        RemainAfterExit = "yes";
        TimeoutStartSec = infinity;
      };
      stopIfChanged = true;
      after = [ "lxd.service" ];
      requires = [ "lxd.service" ];
      requiredBy = [ "lxd.target" ];
      partOf = [ "lxd.target" ];
      script = ''
        lxc="${lxdBinDir}/lxc ${commonLxdArgs}"
        target=$(${makeImageHash image})
        echo "target image hash: '$target'"
        ${imageStartScript name image}
      '';
      preStop = ''
        lxc="${lxdBinDir}/lxc ${commonLxdArgs}"
        ${imageStopScript name image}
      '';
    };
  };

  projectService = name: project: let
    unitName = projectUnitName name;
    projectConfig = lxdConfig unitName project.config;
  in lxdService  {
    inherit name unitName;
    inherit (project) persistent overwrite implicit;
    type = "project";
    description = "LXD project ${name}";
    target = "${projectUnitName name}.target";
    extraArgs = "--project ${name}";
    createScript = ''
      echo "creating project..."
      $lxc project create ${name}
      $lxc project edit ${name} < ${projectConfig}
    '';
    setupScript = optionalString (project.config != null) ''
      echo "applying project configuration..."
      $lxc project edit ${name} < ${projectConfig}
    '';
    deleteScript = ''
      echo "attempting to delete project..."
      $lxc project delete ${name} || true
    '';
  };

  profileService = project: name: profile: let
    unitName = profileUnitName project name;
    profileConfig = lxdConfig unitName profile.config;
    projectService = "${projectUnitName project}.service";
    # storage
    storageDevice = device: device.type == "disk" && hasAttr "pool" device;
    storageDevices = filter storageDevice (attrValues profile.config.devices or {});
    storagePoolService = device: "${storageUnitName device.pool}";
    storagePoolServices = map storagePoolService storageDevices;
  in lxdService  {
    inherit name unitName;
    inherit (profile) persistent overwrite implicit;
    type = "profile";
    description = "LXD profile ${project}/${name}";
    dependencies = [ projectService ] ++ storagePoolServices;
    target = "${projectUnitName project}.target";
    extraArgs = "--project ${project}";
    createScript = ''
      echo "creating profile..."
      $lxc profile create ${name}
      $lxc profile edit ${name} < ${profileConfig}
    '';
    setupScript = optionalString (profile.config != null) ''
      echo "applying profile configuration..."
      $lxc profile edit ${name} < ${profileConfig}
    '';
    deleteScript = ''
      echo "attempting to delete profile..."
      $lxc profile delete ${name} || true
    '';
  };

  containerService = project: name: container: let
    # unit/project/config
    unitName = containerUnitName project name;
    stopTimeout = toString(container.stopTimeout);
    projectService = "${projectUnitName project}.service";
    containerConfig = lxdConfig unitName (lxdContainerConfig image container);
    # image
    ownImage = hasOwnImage container;
    baseImage = getImage project name container;
    imageName = baseImage.name; image = baseImage.value;
    imageService = "${imageUnitName imageName}.service";
    imageServices = optional (! ownImage) imageService;
    # storage
    storageDevice = device: device.type == "disk" && hasAttr "pool" device;
    storageDevices = filter storageDevice (attrValues container.config.devices or {});
    storagePoolService = device: "${storageUnitName device.pool}";
    storagePoolServices = map storagePoolService storageDevices;
    # profiles
    profileService = profile: "${profileUnitName project profile}.service";
    profileServices = map profileService container.profiles;
    relatedServices = storagePoolServices ++ imageServices ++ profileServices;
    # shell
    nixosRootShell = "/run/current-system/sw/bin/bash -l";
    defaultRootShell = if image.type == "nixos" then nixosRootShell else "/bin/sh";
    rootShell = if isNull image.rootShell then defaultRootShell else image.rootShell;
    # script fragments
    consoleScript = "$lxc console ${name} --show-log | tail -n+3";
    setFailed = "$lxc config set ${name} user.volatile.failed true";
    checkFailed = "$lxc config get ${name} user.volatile.failed | grep -q '^true$'";
    runningCheck = "$lxc info ${name} | grep -qP '^Pid: |0-9]+$'";
    bootWaitScript = ''
      $lxc exec ${name} -- ${rootShell} -c '${image.bootWaitScript}' | \
      grep -qP '^(running|degraded)'
    '';
    failStopScript = if ! container.keepFailed then ''
      echo "force-fully stopping container..."
      $lxc stop --force ${name} || true
    '' else ''
      echo "marking container as failed, will delete at next start"
      ${setFailed}
    '';
    resetScript = if container.persistent then ''
      systemd-cat -p warning <<EOF
      The definition of the container has changed, but this container is
      persistent, so it will keep using the old image.
      EOF
    '' else ''
      echo "stopping container..."
      $lxc stop ${name} --timeout ${stopTimeout} >/dev/null 2>&1 || true
      echo "deleting container..."
      $lxc delete ${name} --force
    '';
  in lxdService  {
    type = "container";
    inherit name unitName;
    inherit (container) overwrite implicit;
    description = "LXD container ${project}/${name}";
    serviceConfig = {
      #XXX: needs systemd v244, wait for nixos 20.03
      #XXX: need to find a way to limit restart count
      #Restart = if container.restartFailed then "on-failure" else "no";
      TimeoutStartSec = image.bootWait * 2;
      TimeoutStopSec = (max 5 container.stopTimeout) * 2;
    };
    unitConfig = {
      RestartSec = container.restartDelay;
    };
    dependencies = [ projectService ] ++ relatedServices;
    target = "${projectUnitName project}.target"; extraArgs = "--project ${project}";
    persistent = container.cached or container.persistent;
    commonScript = ''
      target=$(${makeImageHash image})
      if ! $lxc config show >/dev/null 2>&1; then
        echo "ERROR: failed to reach LXD daemon (error $?), check service permissions"
        exit 76
      fi
    '';
    cleanupScript = ''
      active=$($lxc config get ${name} volatile.base_image)
      echo "active base image hash: '$active'"
      if [ "$active" != "$target" ]; then
        echo "image hash changed, need rebuild"
        ${resetScript}
      fi
    '';
    createScript = ''
      echo "creating container..."
      ${optionalString ownImage (imageStartScript imageName image)}
      $lxc init ${imageName} ${name} < ${containerConfig}
      ${optionalString ownImage (imageStopScript imageName image)}
      $lxc config set ${name} volatile.base_image "$target"
    '';
    setupScript = ''
      echo "applying container configuration..."
      $lxc config edit ${name} < ${containerConfig}
    '';
    startScript = ''
      if ${checkFailed}; then
        echo "stopping failed container..."; $lxc stop --force ${name} || true
        $lxc config unset ${name} user.volatile.failed
      fi
      if ! ${runningCheck}; then echo "starting container..."; $lxc start ${name}
      else echo "container already started"; fi
      error () {
        set +e;
        ${failStopScript}
      }; trap "error 76" ERR;
      echo "waiting for system startup..."
      i=0; while [ $i -lt ${toString(image.bootWait)} ]; do
        if ! ${runningCheck}; then
          echo "ERROR: container panic, lost PID1 during boot";
          error 71
        fi
        (${bootWaitScript}) && break || sleep 1 ; ((i+=1))
      done
      echo "patching system status..."
      $lxc exec ${name} -- ${rootShell} -c '${image.bootPatchScript}'
      echo "checking system status :"
      $lxc exec ${name} -- ${rootShell} -c '${image.checkScript}'
      ${optionalString container.logConsole consoleScript}
    '';
    stopScript = ''
      if ${runningCheck}; then
        echo "stopping container..."; $lxc stop ${name} --timeout ${stopTimeout}
        ${optionalString container.logConsole consoleScript}
      fi
    '';
    deleteScript = ''echo "deleting container..."; $lxc delete ${name}'';
  };

  projectTarget = name: project: {
    name = projectUnitName name;
    value = {
      description = "LXD project ${name}";
      partOf = [ "lxd.target" ];
      wantedBy = [ "lxd.target" ];
    };
  };

  # Option types
  # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  storageType = with types; submodule { options = {
    driver = mkOption {
      description = "Storage driver name.";
      type = str;
    };
    config = mkOption {
      description = "Storage driver arguments.";
      type = attrsOf str;
      default = {};
    };
    persistent = mkOption {
      description = "Never delete this storage backend, even if it's not used anymore.";
      type = bool;
      default = false;
    };
    implicit = mkOption {
      description = "Expect this storage backend to already exist, and do not configure it.";
      type = bool;
      default = false;
    };
    overwrite = mkOption {
      description = "If a storage backend with the same name exists, reuse it.";
      type = bool;
      default = false;
    };
  };};

  projectType = with types; submodule { options = {
    config = mkOption {
      description = "Profile configuration.";
      type = nullOr attrs;
      default = {};
    };
    profiles = mkOption {
      description = "Profiles";
      type = attrsOf profileType;
      default = { "default" = { implicit = true; }; };
    };
    containers = mkOption {
      description = "Containers";
      type = attrsOf containerType;
      default = {};
    };
    persistent = mkOption {
      description = "Never delete this project, even if it's not used anymore.";
      type = bool;
      default = false;
    };
    implicit = mkOption {
      description = "Expect this project to already exist, and do not configure it.";
      type = bool;
      default = false;
    };
    overwrite = mkOption {
      description = "If a project with the same name exists, reuse it.";
      type = bool;
      default = false;
    };
  };};

  imageType = with types; submodule { options = {
    type = mkOption {
      description = "Image type (LXD remote image, custom tarballs, NixOS images).";
      type = enum ["lxd" "tarball" "nixos"];
      example = literalExample "nixos";
    };
    description = mkOption {
      description = "Description of the image.";
      type = str;
      default = "";
    };
    cached = mkOption {
      description = "Cache the image to avoid rebuilding it on startup.";
      type = bool;
      default = false;
    };
    rootShell = mkOption {
      description = ''
        Environment providing the wait and check commands.
        The default value of this option is determined automatically based
        on the image type ("/run/current-system/sw" for NixOS, "/"
        otherwise).
      '';
      default = null;
      type = nullOr str;
    };
    startWaitCommand = mkOption {
      description = "Command used to wait for the container's environment.";
      type = str;
      default = "/bin/true";
    };
    bootWait = mkOption {
      description = "Time to wait for the container's startup.";
      type = int;
      default = 60;
    };
    bootWaitScript = mkOption {
      description = "Command used to wait for the container's complete boot.";
      type = str;
      default = "systemctl is-system-running";
    };
    bootPatchScript = mkOption {
      description = "Command used to patch the container status before checking.";
      type = str;
      default = "systemctl reset-failed systemd-journald-audit.socket dev-hugepages.mount || true" ;
    };
    checkScript = mkOption {
      description = "Command used to check for the container's status.";
      type = str;
      default = "systemctl status ; systemctl status --failed \\*";
    };
    lxdRemote = mkOption {
      description = "Remote to fetch images from (for the 'lxd' type).";
      type = str;
      default = "images";
    };
    lxdImage = mkOption {
      description = "Name of the image to fetch (for the 'lxd' type).";
      type = str;
    };
    tarballMetadata = mkOption {
      description = "Path to the metadata tarball (for the 'tarball' type).";
      type = str;
    };
    tarballRootfs = mkOption {
      description = "Path to the root tarball (for the 'tarball' type).";
      type = str;
    };
    # XXX: make the default value of this option depend on the container's
    # `persistent` attribute
    nixosPersistent = mkOption {
      description = ''
        Include configuration templates (for the 'nixos' type).
        This option adds a default configuration file in
        `/etc/nixos/configuration.nix` that imports the LXD guest profile
        and values defined from LXD templates.
      '';
      type = bool;
      default = false;
    };
    nixosConfig = mkOption {
      description = "Container's NixOS configuration (for the 'nixos' type).";
      default = { ... }: {};
    };
    nixosShareStore = mkOption {
      description = "Use the host's Nix store (for the 'nixos' type).";
      type = bool;
      default = false;
    };
    # XXX: add NixOS-level warnings about this option
    nixosShareDaemon = mkOption {
      description = ''
        Share the host's Nix daemon (for the 'nixos' type).
      '';
      type = bool;
      default = false;
    };
  };};

  profileType = with types; submodule { options = {
    config = mkOption {
      description = "Profile configuration.";
      type = nullOr attrs;
      default = {};
      example = ''
        {
          devices."root" = {
          type = "disk"; path = "/"; pool = "default";
        };
      '';
    };
    persistent = mkOption {
      description = "Never delete this profile, even if it's not used anymore.";
      type = bool;
      default = false;
    };
    implicit = mkOption {
      description = "Expect this profile to already exist, and do not configure it.";
      type = bool;
      default = false;
    };
    overwrite = mkOption {
      description = "If a profile with the same name exists, reuse it.";
      type = bool;
      default = false;
    };
  };};

  containerType = with types; submodule { options = {
    config = mkOption {
      description = "LXD configuration of the container.";
      type = attrs;
      default = {};
      example = ''
        {
          devices."nic" = {
            type = "nic"; nictype = "p2p";
            host_name = "veth0"; name = "eth0";
          };
        }
      '';
    };
    description = mkOption {
      description = "Description of the container.";
      type = str;
      default = "";
    };
    profiles = mkOption {
      description = "Enabled profiles.";
      type = listOf str;
      default = [ "default" ];
    };
    image = mkOption {
      description = "Image name or definition.";
      type = oneOf [ imageType str ];
    };
    isolated = mkOption {
      description = "Isolate this container.";
      type = bool;
      default = true;
    };
    idmapBase = mkOption {
      description = "Start of range for this container's ID mapping.";
      type = nullOr int;
      default = cfg.idmapBase;
    };
    idmapSize = mkOption {
      description = "Size of range for this container's ID mapping.";
      type = nullOr int;
      default = cfg.idmapSize;
    };
    apparmorProfile = mkOption {
      description = "AppArmor profile name to use with this container.";
      type = nullOr str;
      default = null;
    };
    logConsole = mkOption {
      description = "Log the container's console during start/stop operations.";
      type = bool;
      default = false;
    };
    stopTimeout = mkOption {
      description = "The time to wait before killing a stopping container.";
      type = int;
      default = -1;
    };
    restartFailed = mkOption {
      description = "Automatically restart failed containers.";
      type = bool;
      default = true;
    };
    restartDelay = mkOption {
      description = "Delay between failed container restarts. ";
      type = int;
      default = 5;
    };
    maxRestarts = mkOption {
      description = "Maximum amount of failed container restarts. ";
      type = int;
      default = 3;
    };
    keepFailed = mkOption {
      description = "Keep failed containers running until next start.";
      type = bool;
      default = true;
    };
    cached = mkOption {
      description = "Cache the container to avoid rebuilding it on startup.";
      type = bool;
      default = true;
    };
    persistent = mkOption {
      description = "Never delete this container, even if it has changed.";
      type = bool;
      default = false;
    };
    implicit = mkOption {
      description = "Expect this container to already exist, and do not configure it.";
      type = bool;
      default = false;
    };
    overwrite = mkOption {
      description = "If a container with the same name exists, reuse it.";
      type = bool;
      default = false;
    };
  };};

in {
  # Interface
  # ===========================================================================

  options = with types; {
    virtualisation.lxd = {
      config = mkOption {
        description = "Server configuration.";
        type = attrsOf str;
        default = {};
      };
      storages = mkOption {
        description = "Storage backends";
        type = attrsOf storageType;
        default = {};
      };
      images = mkOption {
        description = "Images";
        type = attrsOf imageType;
        default = {};
      };
      projects = mkOption {
        description = "Projects";
        type = attrsOf projectType;
        default = {};
      };
      apparmor = mkOption {
        description = "AppArmor configuration";
        default = {};
        type = submodule { options = {
          abstractions = mkOption {
            description = "AppArmor abstractions";
            type = attrsOf str;
            default = defaultApparmorAbstractions;
          };
          profiles = mkOption {
            description = "AppArmor profiles";
            type = attrsOf str;
            default = defaultApparmorProfiles;
          };
        };};
      };
      idmapBase = mkOption {
        description = "Start of range for unprivileged ID mapping.";
        type = nullOr int;
        default = null;
      };
      idmapSize = mkOption {
        description = "Size of per-container UID range.";
        type = nullOr int;
        default = null;
      };
    };
  };
  # Implementation
  # ===========================================================================

  config = mkIf cfg.enable {
    assertions = [ {
      assertion = anyImage (i: i.nixosShareDaemon) -> config.nix.useSandbox;
      message = ''
        As containers with a shared daemon can request arbitrary builds from the
        host's Nix daemon, they require the use of the Nix sandbox.
      '';
    } ];
    virtualisation.lxd.storages.default.implicit = true;
    virtualisation.lxd.projects.default.implicit = true;
    systemd.services = let
      baseServices = { lxd = {
        requires = [ "apparmor.service" ]; after = [ "apparmor.service" ];
        preStart = ''
          for path in /var/lib/lxd/server.{crt,key}; do
            if ! [ -s $path ]; then
              echo "WARNING: removing corrupted file at $path"
              rm -f $path;
            fi
          done
        '';
        postStart = ''
          lxc="${lxdBinDir}/lxc ${commonLxdArgs}"
          ${lxdBinDir}/lxd waitready
          echo "applying server configuration..."
          $lxc config edit < ${serverConfig}
        '';
      };};
      storageServices = mapAttrs' storageService cfg.storages;
      imageServices = mapAttrs' storageService cfg.images;
      projectServices = mapAttrs' projectService cfg.projects;
      objectServices = foldAttrs mergeAttrs {} (mapAttrsToList (name: project: (
        (mapAttrs' (profileService name) (project.profiles)) //
        (mapAttrs' (containerService name) project.containers)
      )) cfg.projects);
    in baseServices // storageServices // imageServices // projectServices // objectServices;
    systemd.targets = mapAttrs' (projectTarget) cfg.projects // { lxd = {
      description = "LXD global target";
      wantedBy = [ "multi-user.target" ];
    };};
    security.apparmor.packages = [ apparmorPackage ];
    security.apparmor.profiles = apparmorProfiles;
    users.users.lxd = {
      name = "lxd";
      group = "lxd";
      description = "LXD system user";
      isSystemUser = true;
    };
  };
}

