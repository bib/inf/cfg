{ config, lib, ... }:

{
  services.sanoid = let syncPlan = {
    recursive = true;
    daily = 7;
    hourly = 24;
    monthly = 12;
    yearly = 1;
  }; in {
    enable = true;
    extraArgs = [ "--verbose" ];
    datasets."medix-main/LXD/containers" = syncPlan;
    datasets."medix-cold/BCK/containers" = syncPlan // {
      autosnap = false;
      autoprune = true;
    };
    datasets."medix-main/LXD/custom" = syncPlan;
    datasets."medix-cold/BCK/custom" = syncPlan // {
      autosnap = false;
      autoprune = true;
    };
    datasets."medix-main/HOT/custom" = syncPlan;
    datasets."medix-cold/BCK/custom-hot" = syncPlan // {
      autosnap = false;
      autoprune = true;
    };
    datasets."medix-cold/LXD/custom" = syncPlan;
  };
  services.syncoid = {
    enable = true;
    commonArgs = [ "--no-sync-snap" ];
    commands."medix-main/LXD/containers" = {
      target = "medix-cold/BCK/containers";
      recursive = true;
    }; 
    commands."medix-main/LXD/custom" = {
      target = "medix-cold/BCK/custom";
      recursive = true;
    };
    commands."medix-main/HOT/custom" = {
      target = "medix-cold/BCK/custom-hot";
      recursive = true;
    };
  };
}

