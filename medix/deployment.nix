{ config, pkgs, lib, ... }:
with lib; with types; let

  # Backend option mapping
  # ---------------------------------------------------------------------------

  httpBackendOptionType = { options = {
    enable = mkOption {
      default = true;
      type = bool;
      description = "Enable HTTP forwarding to this backend.";
    };
    config = mkOption {
      default = {};
      type = attrs;
      description = "Extra HAProxy configuration of this backend.";
    };
  };};

  mailBackendOptionType = { options = {
    enable = mkOption {
      default = false;
      type = bool;
      description = "Enable SMTP/IMAP forwarding to this backend.";
    };
    smtpConfig = mkOption {
      default = {};
      type = attrs;
      description = "Extra nginx SMTP configuration of this backend.";
    };
    imapConfig = mkOption {
      default = {};
      type = attrs;
      description = "Extra nginx IMAP configuration of this backend.";
    };
  };};

  backendOptionType = { options = {
    # Enabled/disabled backends
    enable = mkOption {
      default = true;
      type = bool;
      description = "Enable/disable this backend";
    };
    # Backend matched paths
    domains = mkOption {
      default = [];
      type = listOf str;
      description = "Domains to match for this service's frontend.";
    };
    # Backend IP address
    address = mkOption {
      type = str;
      description = "IPv4 address of this service's backend.";
    };
    # HTTP backend configuration
    http = mkOption {
      type = submodule httpBackendOptionType;
      default = { enable = true; };
      description = "HTTP forwarding options";
    };
    # HTTP backend configuration
    mail = mkOption {
      type = submodule mailBackendOptionType;
      default = {};
      description = "SMTP/IMAP forwarding options";
    };
  };};

  # Service option mapping
  # ---------------------------------------------------------------------------

  serviceOptionType = { options = {
    # ACME certificate options
    certificate = mkOption {
      default = null;
      type = nullOr attrs;
      description = ''ACME renewal options (see security.acme.certs.*).'';
    };
    # Backends option
    backends = mkOption {
      default = {};
      type = attrsOf (submodule backendOptionType);
      description = "Backend definitions.";
    };
    wildcard = mkOption {
      default = false;
      type = bool;
      description = "Use a wildcard certificate for this domain's subdomains.";
    };
  };};

in {
  options = {

  # Deployment options
  # ---------------------------------------------------------------------------

    security.acme.webroot = mkOption {
      default = "/var/www/";
      type = str;
      description = "Default webroot path for ACME web challenges.";
    };

    deployment.externalIPv4 = mkOption {
      default = {};
      type = str;
      description = "External IPv4 address on which the services should be exposed.";
    };

    deployment.internalIPv4 = mkOption {
      default = {};
      type = str;
      description = "Internal IPv4 address used to communicate with the backends.";
    };

    deployment.admins = mkOption {
      default = {};
      type = attrs;
    };

    deployment.services = mkOption {
      default = {};
      type = attrsOf (submodule serviceOptionType);
      description = ''
        These definitions are used to generate :
        <itemizedlist>
          <listitem><para>
            ACME certificates alternate names (`security.acme.certs`)
          </para></listitem>
          <listitem><para>
            Reverse-proxy backend definitions (`services.haproxy.config`)
          </para></listitem>
          <listitem><para>
            Container interface definitions (`systemd.network.networks`)
          </para></listitem>
          <listitem><para>
            Private DNS records for each container (`networking.hosts`)
          </para></listitem>
          <listitem><para>
            Public DNS records for each domain (`networking.hosts`)
          </para></listitem>
        </itemizedlist>
      '';
    };

    deployment.containerMap = mkOption {
      type = attrs;
      internal = true;
      visible = false;
    };

  };

  config = {
    deployment.containerMap = let
      getAddresses = _: backend: backend.address;
      getDomainMap = defn: mapAttrs getAddresses defn.backends;
    in fold mergeAttrs {} (map getDomainMap (attrValues config.deployment.services));

    environment.systemPackages = let mapFile = pkgs.writeText "bib-container-map" (
      concatStringsSep "\n" (mapAttrsToList (name: address: "${name} ${address}") config.deployment.containerMap)
    ); in [ (pkgs.writeShellScriptBin "bib-launch-service" ''

        # Usage
        # -----
        if [[ -z "$1" || -z "$2" ]]; then
          echo "Usage: bib-launch-service <BASE_IMAGE> <SERVICE_NAME>" >&2
          exit 0
        fi

        # Globals
        # -------

        IMAGE=$1
        CONTAINER=$2
        SELF=`basename "$0"`
        OSNAME=$(echo "$IMAGE" | cut -d- -f2)
        ADDRESS=$(grep "^$CONTAINER" ${mapFile} | cut -d' ' -f2)
        IFNAME="lxd-$CONTAINER"

        BRed='\033[1;31m'         # Red
        BGreen='\033[1;32m'       # Green
        Cyan='\033[0;36m'         # Cyan
        BWhite='\033[1;37m'       # White
        Reset='\033[0m'           # Text Reset

        message() {
          echo -e "''${!1}$2''${Reset}" >&2
        }

        # Sanity checks
        # -------------

        if [[ -z $ADDRESS ]]; then
          message BRed "### ERROR: service '$2' not found in the service map."
          message BWhite "### make sure it has been defined in /etc/nixos/services.nix."
        exit 1
        fi

        if lxc info $CONTAINER >/dev/null 2>&1; then
          message BRed "### ERROR: container '$2' already exists."
          message BWhite "### refusing to overwrite existing container."
        exit 1
        fi

        # Error handler
        # -------------

        cleanup() {
          message BRed "### ERROR: failed to launch service container."
          exit 1
        }
        trap cleanup ERR

        # Script body
        # -----------

        message BWhite "### launching new container..."
        lxc launch $IMAGE $CONTAINER -c user.ipv4_address=$ADDRESS 2>/dev/null
        message BWhite "### adding uplink interface..."
        lxc config device add $CONTAINER uplink nic nictype=p2p host_name=$IFNAME name=eth0 mtu=1420
        message BWhite "### restarting container..."
        lxc restart --force $CONTAINER
        message BWhite "### waiting for container startup..."
        while true; do
          # ignore the state of the systemd-journald-audit socket, as it's broken in Debian images
          lxc exec $CONTAINER -- /bin/sh -lc "systemctl reset-failed systemd-journald-audit.socket" >/dev/null 2>&1 || true
          state=$(lxc exec $CONTAINER -- /bin/sh -lc "systemctl is-system-running" 2>/dev/null || true)
          [[ "$state" = "running" ]] && break
          message Cyan "waiting ($state)..."
          sleep 0.5
        done
        if [[ "$OSNAME" = "nixos" ]]; then
          message BWhite "### rebuilding NixOS configuration..."
          lxc exec $CONTAINER -- /bin/sh -lc "nixos-rebuild switch"
        fi
        message BGreen "### container launched at address $ADDRESS !"
    '') ];

    systemd.services."push-containers-keys" = {
      description = "Send admin ssh keys to containers";
      serviceConfig = {
        Type = "oneshot";
      };
      script = let
        admins = [ "benbart" "pistache" "smyds"];
        adminKeys = concatMap (user: config.users.users.${user}.openssh.authorizedKeys.keys) admins;
        authorizedKeysFile = pkgs.writeText "adminKeys" (concatStringsSep "\n" adminKeys);
      in ''
          lxc profile default set default user.authorized_keys="${concatStringsSep " " adminKeys}"
         '';

#concatStringsSep "\n" (mapAttrsToList (name: file: "if lxc info ${name} >/dev/null 2>&1; then lxc file push ${file} ${name}/root/.ssh/authorized_keys2; fi") authorizedKeysFiles);
    };

  };
}

