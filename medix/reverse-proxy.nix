{ config, pkgs, lib, ... }:
with lib; with strings;

let

  # Function definitions
  # ===================================================================================================================

  # Generic utilities
  # -------------------------------------------------------------------------------------------------------------------

  # Determine
  notEmpty = string: string != "";

  # Build a FQDN from a domain and subdomain, where the subdomain may be empty
  makeFQDN = domain: subdomain: concatStringsSep "." (filter notEmpty [ subdomain domain ]);

  # Filter backends to keep only those with configured subdomains
  filterBackends = filterAttrs (name: backend: backend.http.enable && (length backend.domains) != 0);

  # Format haproxy option mappings
  formatOptions = let
    formatValue = value: if isBool value then "" else toString value;
    formatOption = path: value: concatMapStringsSep " " formatValue (filter notEmpty (path ++ [value]));
    traverse = path: mapAttrsToList (name: value: visit (path ++ [ name ]) value);
    visit = path: opts: if isAttrs opts then traverse path opts else formatOption path opts;
  in opts: flatten (visit [] opts);

  # Haproxy ACLs
  # -------------------------------------------------------------------------------------------------------------------

  # Get only the domain part in a full path
  getDomain = path: elemAt (splitString " " (elemAt (splitString "/" path) 0)) 0;
  # Build a file containing the matched domains for an ACL
  makeMatchFile = name: paths: pkgs.writeText "haproxy-acl-${name}" (concatMapStringsSep "\n" getDomain paths);
  # Build an haproxy ACL for all the subdomains of a backend
  makeBinding = name: backend: concatStringsSep "\n  " [
    "use_backend ${name} if { base_beg -i -f ${makeMatchFile name backend.domains} }"
    "use_backend ${name} if { base_dom -i ${name} }"
  ];
  # Build haproxy ACLS for all the backends of a domain definition
  makeDomainBindings = domain: def: concatStringsSep "\n  " (mapAttrsToList makeBinding (filterBackends def.backends));
  # Build haproxy ACLs for all of the given domain definitions
  haproxyBindings = concatStringsSep "\n  " (mapAttrsToList makeDomainBindings config.deployment.services);

  # Haproxy backends
  # -------------------------------------------------------------------------------------------------------------------

  # Build an haproxy backend with the given name and address
  makeBackend = name: backend: concatStringsSep "\n  " (
    [ "backend ${name}" "mode http" "http-request set-header X-Forwarded-Port %[dst_port]" "http-request add-header X-Forwarded-Proto https if { ssl_fc }" ]
    ++ (formatOptions backend.http.config)
    ++ [(concatStringsSep " " (["server" name "${backend.address}:80"]))]
  );
  # Build haproxy backends for all the backends of a domain definition
  makeDomainBackends = domain: def: concatStringsSep "\n" (mapAttrsToList makeBackend (filterBackends def.backends));
  # Build haproxy backends for all of the given domain definitions
  haproxyBackends = concatStringsSep "\n" (mapAttrsToList makeDomainBackends config.deployment.services);

  # ACME certs
  # -------------------------------------------------------------------------------------------------------------------

in {

  # Configuration values
  # ===================================================================================================================

  # ACME certs
  # -------------------------------------------------------------------------------------------------------------------


  # Reverse-proxy configuration
  # -------------------------------------------------------------------------------------------------------------------

  services.haproxy = {
    # TCP reverse proxy
    enable = true;
    config = ''
      # Global configuration
      global
        stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
        #log /dev/log    local0 info alert
        log /dev/log    local1 notice alert  #info alert
        maxconn 20000
        tune.ssl.default-dh-param 2048
        #tune.bufsize 65536

      defaults
        mode http
        log global
        option httplog
        # option http-server-close
        option forwardfor
	# option http-buffer-request
        timeout connect 15s
        timeout client 40s
        timeout client-fin 40s
        timeout server 10s
        timeout tunnel 1h

      # HTTP/HTTPS frontends
      frontend http
        bind 0.0.0.0:80
        ${haproxyBindings}

      # Generated backends
      ${haproxyBackends}
    '';
  };
}

