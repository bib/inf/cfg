# medix.lebib.org -- main configuration file
# ============================================================================

{ config, pkgs, lib, ... }:

{
  imports = [
    # System modules
    # -------------------------------------------------------------------------

    ./hardware-configuration.nix   # Auto-generated hardware configuration
    ./kernel.nix                   # Bootloader/kernel configuration
    ./system.nix                   # System packages/services
    ./users.nix                    # Local users
    ./networking.nix               # Networking configuration
    ./firewall.nix                 # Firewall configuration
    ./snapshots.nix                # Snapshotting configuration

    # Local services
    # -------------------------------------------------------------------------

    ./dns-recursor.nix             # DNS recursor
    ./reverse-proxy.nix            # HTTP/TCP reverse proxy
    ./haproxy.nix                  # Haproxy module override
    ./virtualisation.nix           # Virtualisation configuration

    # Deployment configuration
    # -------------------------------------------------------------------------

    ./deployment.nix               # Deployment option definitions

  ];


  security.auditd.enable = true;
  security.audit.rules = [
   "-w /dev/null  -p a -k dev-null-changed"
  ];

}

