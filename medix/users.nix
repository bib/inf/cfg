{ config, pkgs, lib, ... }:

with lib;

let
  ssh_keys = {};
in

{
  security.sudo.wheelNeedsPassword = false;

  users.motd = ''
+---------------+--------------------------------------------------------------+
|  NixOS 21.05  |  medix.lebib.org                                             |
|   \\  \\ //   |    Admins      : pistache, benbart, smyds, wargreen          |
|  ==\\__\\/ // |    Services    : LXD host, WireGuard, BIRD, haproxy          |
|    //   \\//  |    Router-ID   : 10.13.255.221                               |
| ==//     //== |    CPU         : 4 vCPU cores (Intel i7-870  @ 2.93GHz)      |
|  //\\___//    |    Memory      : 4 GiB                                       |
| // /\\  \\==  |    Storage     : main:100GB/HDD / cold:500GB/HDD             |
|   // \\  \\   |                                                              |
+---------------+--------------------------------------------------------------+
  '';

  users.users = (mapAttrs (name: admin: {
    isNormalUser = true;
    extraGroups = [ "wheel" "lxd" ];
    openssh.authorizedKeys.keys = admin.ssh_keys;
  }) config.deployment.admins) // {
    root.openssh.authorizedKeys.keys = flatten (mapAttrsToList (name: admin:
      map (line: "${line} (${name})") admin.ssh_keys
    ) config.deployment.admins);
  };
}

