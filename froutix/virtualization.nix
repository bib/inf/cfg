{ config, lib, pkgs, ... }:

{
  imports = [
    (import (builtins.fetchGit {
      url = https://framagit.org/bib/dev/nix/libvirt-domains.git;
      rev = "ce541341f162b4f4b1735b468199cab5d511ab2e";
    }))
  ];

  environment.shellAliases.virsh = "virsh --connect=qemu:///system";
  systemd.services.libvirtd.serviceConfig.LimitMEMLOCK = "infinity";

  virtualisation = {
    libvirtd = {
      enable = true;
      # Allow UEFI VMs
      qemuOvmf = true;
      # On shutdown, use managedsave to "suspend" the VMs
      onShutdown = "suspend";
      # QEMU settings (enable SPICE and tweak the sandboxing)
      qemuVerbatimConfig = ''
        # spice_listen = "0.0.0.0"
        # spice_tls = 1
        # spice_tls_x509_cert_dir = "/etc/pki/libvirt-spice"
        cgroup_device_acl = [
          "/dev/null", "/dev/full", "/dev/zero",
          "/dev/random", "/dev/urandom", "/dev/ptmx",
          "/dev/kvm", "/dev/kqemu",
          "/dev/rtc", "/dev/hpet", "/dev/net/tun",
          "/dev/vfio/vfio", "/dev/vfio/2", "/dev/vfio/11"
        ]
        seccomp_sandbox = 0
      '';

      domains = let
        mkMainDisk = domain: name:
          { source.file = "/srv/kvm/froutix-main/${domain}/${name}";
            boot = { enable = true; order = 2; };
            driver.cache = "writeback";
            address.unit = 0; target = "hda"; };
        setupDisk =
          { type = "cdrom"; boot = { enable = true; order = 1; };
            address.unit = 1; target = "hdb"; };
        mkColdDisk = domain: name:
          { source.file = "/srv/kvm/froutix-cold/${domain}/${name}";
            driver.cache = "writeback";
            address.unit = 3; target = "hdd"; };
      in let
        mkDisks = name: domain: [ (mkMainDisk domain name) (setupDisk)
                     (mkColdDisk domain name) ];
        mkInterface = macAddress: bridge: {
          inherit macAddress; source = { inherit bridge; }; address.slot = "0x3";
        };
      in {

        # Medix
        medix = {
          # Machine metadata
          hostname = "medix";
          type = "kvm";
          domain = "lebib.org";
          description = ''
            Type     : KVM (IOMMU/VT)
            Address  : 10.13.221.254
            Guest OS : nixOS GNU/Linux 21.05 (Okapi)
            CPU      : 4 cores
            Memory   : 4Go (max 4Go)
            Storage  : main:80Go/HDD cold:512Go/HDD
          '';
          # CPU configuration
          vcpu = 4;
          cpuset = "0,1,2,3";
          cputune.vcpupins = { "0" = 0; "1" = 1; };
          # Memory configuration
          memory = 4096;
          # Network configuration
          interfaces = [ (mkInterface "52:54:00:a4:ef:ba" "br-medix") ];
          # Storage configuration
          disks = mkDisks "medix" "lebib.org";
        };

        # Storix
        storix = {
          # Machine metadata
          hostname = "storix";
          type = "kvm";
          domain = "lebib.org";
          description = ''
            Type     : KVM (IOMMU/VT)
            Address  : 10.13.222.254
            Guest OS : nixOS 21.05 (Okapi)
            CPU      : 4 cores
            Memory   : 4Go (max 4Go)
            Storage  : main:80Go/HDD cold:512Go/HDD
          '';
          # CPU configuration
          vcpu = 4;
          cpuset = "4,5,6,7";
          cputune.vcpupins = { "0" = 0; "1" = 1; };
          # Memory configuration
          memory = 4096;
          # Network configuration
          interfaces = [ (mkInterface "52:54:00:a4:ef:ca" "br-storix") ];
          # Storage configuration
          disks = mkDisks "storix" "lebib.org";
        };
      };
    };
  };
}

