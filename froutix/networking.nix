{ config, pkgs, lib, ... }:
{

  networking = {
    hostName = "froutix";
    hostId = "13acab12";
    useDHCP = false;
    wireguard.enable = true;
    firewall.enable = false;
    nameservers = [ "127.0.0.1" ];
    iproute2.enable = true;
    iproute2.rttablesExtraConfig = ''
      22400 internal
    '';
  };

  systemd.network.enable = true;
  systemd.network.netdevs = {
    # Uplink tunnel to `bibix.lebib.org`
    tun-mgmt = {
      netdevConfig = {
        Kind = "wireguard";
        Name = "tun-mgmt";
      };
      wireguardConfig = {
        PrivateKeyFile = "/etc/keys/wireguard/tun-mgmt/froutix.priv";
        FirewallMark = 13248;
        ListenPort = 53248;
      };
      wireguardPeers = [
        { wireguardPeerConfig = {
            Endpoint = "91.169.136.10:53248";
            AllowedIPs = [
              "10.13.0.0/16"     # allow using as default gateway
              "10.17.0.0/16"     # allow using as default gateway
              "224.0.0.0/8"   # allow OSPF traffic
            ];
            PublicKey = "eBr8fXp2+6PU6FsyMOdWxPz94+sw2cdRFlUougLIml8=";
            PersistentKeepalive = 30;
        }; }
      ];
    };

    # Uplink bridge (@BIB)
    br-uplink-1 = {
      netdevConfig = { Kind = "bridge"; Name = "br-uplink-1"; };
      extraConfig = ''
        [Bridge]
        STP=off
      '';
    };

    # Downlink bridge to `medix.lebib.org`
    br-medix = {
      netdevConfig = {
        Kind = "bridge";
        Name = "br-medix";
      };
      extraConfig = ''
        [Bridge]
        STP=off
      '';
    };

    # Downlink bridge to `storix.lebib.org`
    br-storix = {
      netdevConfig = {
        Kind = "bridge";
        Name = "br-storix";
      };
      extraConfig = ''
        [Bridge]
        STP=off
      '';
    };
  };


  systemd.network.networks = {
    # Loopback interface
    loopback = {
      name = "lo";
      address = [ "127.0.0.1/8" "10.13.255.202/32" ];
    };
    # Uplink tunnel to `bibix.lebib.org`
    tun-mgmt = {
      name = "tun-mgmt";
      addresses = [
        { addressConfig = {
          Address = "10.13.255.202/32"; Peer = "10.13.255.208/32"; }; }
      ];
      routes = [
        { routeConfig = {
          Destination = "10.13.0.0/16"; Scope = "link"; }; }
        { routeConfig = {
          Destination = "10.17.0.0/16"; Scope = "link"; }; }
      ];
    };

    # Uplink bridge (@BIB)
    br-uplink-1 = {
      name = "br-uplink-1";
      addresses = [
        { addressConfig = { Address = "192.168.255.202/24"; }; }
      ];
      gateway = [ "192.168.255.254" ];
    };
    enp0s25 = { name = "enp0s25"; bridge = [ "br-uplink-1" ]; };
    eno1 = { name = "eno1"; bridge = [ "br-uplink-1" ]; };

    # Downlink bridge to `medix.lebib.org`
    br-medix = {
      name = "br-medix";
      addresses = [
        { addressConfig = { Address = "10.13.255.202/32"; Peer = "10.13.255.221/32"; }; }
      ];
      routes = [
        # Route to medix containers
        { routeConfig = { Destination = "10.13.221.0/24"; Scope = "link"; }; }
      ];
      linkConfig.RequiredForOnline = false;
    };
    # Downling bridge to `storix.lebib.org`
    br-storix = {
      name = "br-storix";
      addresses = [
        { addressConfig = { Address = "10.13.255.202/32"; Peer = "10.13.255.222/32"; }; }
       ];
      routes = [
        # Route to storix containers
        { routeConfig = { Destination = "10.13.222.0/24"; Scope = "link"; }; }
      ];
      linkConfig.RequiredForOnline = false;
    };
  };
}
