{ config, lib, pkgs, ... }:

let
  ssh_keys = {
    benbart = [
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiLtjyaq5VAFIQwEy+HJa5Hf4a8/zHuwp3nuW5LXCiQwAAt+/byOkgqFUjF7I0bsYIIaoMEbCCjkIKfpvX+/wZqBJvJ4fpvqaK48yl8Qpir+VPmgruEInF5Ovm696q2Fj/fHTEzr9NO5uKUfgdnyvYr0L9HVZMp+PmgHnyvBMo5LvpQ/HS8ayXTL8dyk+GfYqjtdzR0kTF0YXZ4udRSlXCazUEDc9GbQD6aoOdiWnnDQ++j5AN/4LTd1gluC9ywVo3/AWBSxQe/d+UQm8GOaM4QXLNmO5C5XQS4jKznir/s7MHkd0WzaWxFux5+7IkXqtkx/omPlZEs/cKLob648If cardno:000500005F42" 
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC7RidQzA/IF7mkvquIgivY4qIHL0v3piJagB01NkaVJF6e3fX1QBOPt4Nzx3lihRs4WVlBaPNZ/mJaNvPqdG+1VdWd/7Hgktg7D/7cILquUlzWCp7v2eJt1uvkLsr3JjB/bJ9rIb96QpsGP+ftddwpRgybLVVGcVU3tTF8t+ZrOkZ3UhSvc5xa/NppbFaHDZbHqAbiirLxJKr54RKkuiKpqb/YCJM+lR0qf6sxtqOL2ZsaO8D9G2GxsmBq0tkjakmJ6oO0C7xzF2StR6DpPykKT4gBy64duntkHbBzP0cIE8nrJcB4B9baHv+2EmcC0LSu0QDJ8FfJFba3O2GdCefpYsoGJ3XszfyFG9IIEddtlMrxuEYBdDMLkoKFmqRW7uftWJgJAtnimQPFyvS4oov4cDlfbDIXjJNHoUP+mEz36UBP7J3IETbhgXRv03v86FhHoHPZVXFOPhsZDVkQR+JLWlpOUEaFLCv+4D7oxKqWbLOL6At5ELE9lsIIY9kwZBBIF+Ju/JWv6y5eKlWUukbzHOQJQ/iTL4TK0WCrPt2oUiDwIXMfIbogRASJ1KzCQcncSJXuNx9jvaq08COVhVixHrVsu3T71sCvqkYBc4ePHeZfeea5Rl5WRJhNL7Tt6J6fqvmAynYWmJtaz/PdeBn0DbhamFrgROgLksyOg3QfJQ== bchatelain@pc46" ];
    pistache = [ 
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDAP7dxz43XD7OVFHLRIvp7QLNk3QD8VCuWkOYsrfFLp3gX9mD5/YIGp0pfAUG2eZjwcA1ef0fCERChwUqO4Wrz0xaQ1PD7hlC+6XqDCwCDNEGnufOZbilQxlpuroZUQ/TEVhbHYxTK2hyV63eVvDh56dI3IqeoxqU1oY5BnXZjHJYkWPbkIxhsgGTlW5nJfkgZyKDie1O79NJdzQmqVuZt5z6yKhnuX2+gOgRC5hfto8tWLZj9Hy9d15VKL1b0VfuCK+mpANRhQds7YsfMfTTYMIDfuFCI8ZpYWQxNw7t8QXbfLaMsiRgphGZEr555g+xdkXCZ2+Oj4rlPfKgMzXLQ2gFv0Aen4wwzEdjhXdiwuKbbbsI4WKd3kvX5Nc70wKYxvI8fQs0GlO9FLdEbC8R6J15JKREmJlgizImmqb8KIME4bGsYGOVPFVbRHQKjUT1rFVWD3+aDml80cAlHOpfoln7C4oqCi+oDDmAuyH358O2S4EdIGSGe4I2i0eJvxn3EA8ZVLKUWj/lcyJ05eFHdLZWcLyC4op8faNGqhdgG0I/tRJ/sFgpCj0Z7b5afMKxdX/LHYtOqcpxoQpT/mwIEQe6VPpUHQC7xrOB/kKIc5DNSVZdIQnaqRYvUrgPrWhXR9MG3/7Gt4lq0j39n1+Fm7aj6L9CmLHc3vfwCttv/9Q== openpgp:0x451F2F0F" ];
    smyds = [
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCoerbEBysWo+cJ6SfWTJuCadhJcwGvPdsGF4dmzJrK/X5v8impivie1s7iS8dfYUsUgiAolVqaKAa1cE+E5J5VHRv+bMbpbUjeLR3YCtBtCnr4Zp9YOwsgPH7QN6O7kuZTv9RaoXbDs/OVM1UnYIzFVwoBiiEokqBgCwy0RYo400raFJ+xRk4x/qyUsYLn1jxo3h/z3IK1V2BuaXEa2g7n1/CG6wSgIZ5z6TiCWp0sRFLWpfmqAoWML71SdLl+/opy57zrPO0bUtCKFzupTo3MzyQdaFavm5OXuId5y1qm4zi/X6fHTljwU3igkvmvsjjCvxTbamgouxyhEl5b2HnX smyds@riseup.net"];
    wargreen = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBRUVHxWsppYlotxeFzTA8HHROSDW/P6nKjAeD9aqUR+QVhiK88mBmTAYjjPbWrxXR214O3u45I5RykyNks5llfYndGcb0tuapZU8T/ELQBHNfAeVl6FxBx4iDYfZ3B3DpSkDk5Rg8HO09qfUAI5S5SUcSIkDQxEufASqBA2cY4KQMgMISz0K4Dy3JelibGBXCCzW+e2ggECyoUfXseQc9Q7CkRH8r2F2xYPQQRA4LWsfcENNzqL/9n737l+QWWLeaJdBx4L+zfYXxNxnciSHctHxXSBUp6WE6yTsA6UzrwfrlDJvmALEQtOa7hlFcc/l9055kmAjRswEe3XUh8rYv wargreen@LaChoze"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXDiGOe86c6RdKktNScQ3VVfhl/dPqJMd9FNtrhgZDLJ3A+0P4E97HVWrahWtYMumsYv133ZdOT1rBNJ61XwqVBNAa22DRg3k0w0ADsMVkOZYr7RbOqbVPT1uL/AFMbo6pDYTYCkVC+/0G2zclOHELaYJg0ZwO1wyMhfmtDWTwxfADiSsb1oq6ve+ybrwc/FMq0iyDRw5zP/8lJGSEGDJTmwbzo54NlMiAVrW+Kp5iVcGk7FGjak3wAiShZTfTiGATDNW/ET1gHTl2m8/QxM04bb5GzTPcXKU+1No4SW3ptAhHPAAB+Y9NgGkZXLpr4euYZDQnz2x+DsTPgxOYbUCZ4OQSBtBVVTAaVj88fLh9/oqmBpD3SSbhjS52q+6sDozDzfMwwjPZ3fHul3ispKbrZVTS6oRbRCyhLCAe6yGH9E+pi1xIaiYzhZQxXGGJos4w9F8BdNxWSAPLsC/3GlSQTcMjAGKa/6fntJ3ZKRXPFU4mcxZEka2kWOlosCDFcTM= wargreen@fetide"
    ];
  };
in

{
  security.sudo.wheelNeedsPassword = false;

  users.motd = ''
    +---------------+--------------------------------------------------------------+
    |  NixOS 21.05  |  froutix.lebib.org, bare-metal host                          |
    |   \\  \\ //   |    Admins      : benbart, pistache                           |
    |  ==\\__\\/ // |    Services    : KVM host, WireGuard, haproxy                |
    |    //   \\//  |    Router-ID   : 10.13.255.202                               |
    | ==//     //== |    CPU         : Intel i7-870 (4c8t) @ 2.9300GHz             |
    |  //\\___//    |    Memory      : 11920 MiB                                   |
    | // /\\  \\==  |    Storage     : HDD SATA (2*320GB),                         |
    |   // \\  \\   |                  HDD SATA (2*1TB + 2*512GB)                  |
    +---------------+--------------------------------------------------------------+
  '';

  users.users.pistache = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.pistache;
  };

  users.users.smyds = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.smyds;
  };

  users.users.benbart = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.benbart;
  };

  users.users.wargreen = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.wargreen;
  };

  users.users.root.openssh.authorizedKeys.keys = lib.concatLists (lib.attrValues ssh_keys);

}

