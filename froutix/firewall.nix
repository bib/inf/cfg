{ config, lib, pkgs, ... }:

{
  boot.kernel.sysctl."net.ipv4.ip_forward" = true;
  services.ferm = {
    enable = true;
    config = ''
      @def $EXT_IF = enp0s25;                                         # External uplink iface
      @def $INT_IF = (br-uplink-1);                        # Internal uplink iface
      @def $INT_ADDR = 192.168.255.202;                                      # Internal uplink address
      @def $VM_IFS = (br-medix br-storix);                              # Backend ifaces
      @def $MGMT_TUN_PORT = 53248;                                     # Management tunnel port
      @def $UPLINK_TUN_PORT = 54224;                                   # Uplink tunnel port
      @def $TUN_PORTS = ($MGMT_TUN_PORT $UPLINK_TUN_PORT);             # Internal tunnel ports
      @def $MGMT_NETS = (10.13.240.240 10.13.248.0/24 10.17.24.3);     # Management nets
      @def $VM_NETS = (10.13.255.221 10.13.255.222);                   # VM nets
      @def $INTERNAL = 10.0.0.0/8;                                     # Internal nets

      table filter {
        chain INPUT {                                                  ### Incoming traffic
          interface lo ACCEPT;                                         # Allow loopback traffic
          proto icmp ACCEPT;                                           # Allow ICMP traffic
          mod state state (RELATED ESTABLISHED) ACCEPT;                # Allow established conns
          mod state state (INVALID) DROP;                              # Drop invalid conns
	  proto tcp dport 6556 ACCEPT;
          saddr $MGMT_NETS ACCEPT;                                     # Allow management traffic
          interface $INT_IF jump INPUT_INTERNAL;                       # Jump to INPUT_INTERNAL
          interface $EXT_IF jump INPUT_EXTERNAL;                       # Jump to INPUT_EXTERNAL
          interface $VM_IFS jump INPUT_VMS;                            # Jump to INPUT_VMS
          saddr $INTERNAL REJECT reject-with icmp-port-unreachable;    # Reject from internal
          policy DROP;                                                 # Drop remaining traffic
        }
        chain INPUT_INTERNAL {                                         ### Internal traffic
          proto udp dport ($TUN_PORTS) ACCEPT;                         # Allow tunnel traffic
          proto tcp dport 22 ACCEPT;                                   # Allow SSH traffic
          jump INPUT_EXTERNAL;                                         # Jump to INPUT_EXTERNAL
        }
        chain INPUT_EXTERNAL {                                         ### External traffic
          proto udp dport 53248 ACCEPT;
          proto tcp dport 22 ACCEPT;                                   # Allow SSH traffic
        }
        chain INPUT_VMS {                                              ### VM traffic
        }
        chain OUTPUT {                                                 ### Outgoing traffic
          policy ACCEPT;                                               # Allow all
        }
        chain FORWARD {                                                ### Forwarded traffic
          mod state state (RELATED ESTABLISHED) ACCEPT;                # Allowed established connections
          mod state state (INVALID) DROP;                              # Drop invalid connections
          saddr $MGMT_NETS ACCEPT;                                     # Allow management traffic
	  saddr 10.17.24.3/32 daddr $VM_NETS proto tcp dport 6556 ACCEPT; 
          interface $INT_IF jump FORWARD_INTERNAL;                     # Jump to FORWARD_INTERNAL
          interface $VM_IFS jump FORWARD_VMS;                          # Jump to FORWARD_VMS
        #  saddr $INTERNAL REJECT reject-with icmp-host-unreachable;    # Reject traffic from internal network
          policy DROP;                                                 # Drop remaining traffic
        }
        chain FORWARD_INTERNAL {                                       # Forwarded internal traffic
          saddr $MGMT_NETS ACCEPT;                                     # Allow management traffic
        }
        chain FORWARD_VMS {                                            # Forwarded VM traffic
          saddr $VM_NETS daddr ! $INTERNAL ACCEPT;                     # Allow VM traffic to external networks
          saddr $MGMT_NETS ACCEPT;                                     # Allow management traffic
        }
      }
      table nat chain PREROUTING interface $INT_IF daddr $INT_ADDR {   ### Incoming local translated traffic
      }
      table nat chain POSTROUTING outerface $INT_IF {                  ### Outgoing forwarded traffic
          saddr $VM_NETS daddr ! $INTERNAL MASQUERADE;                 # Masquerade containers to external
      }
      table nat chain POSTROUTING {
      }
      table mangle chain FORWARD outerface $EXT_IF {                   ### Mangled traffic
      }
    '';
  };
}
