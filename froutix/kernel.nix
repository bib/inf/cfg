{ config, lib, pkgs, ... }:

{
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.supportedFilesystems = [ "zfs" ];
  boot.loader.grub.device = "/dev/sda";
  boot.zfs.requestEncryptionCredentials = true;
  boot.zfs.extraPools = [
    "froutix-cold"
  ];
  boot.kernelParams = [
    "nomodeset"
    "zfs.zfs_arc_max=2147483648"
    "intel_iommu=on" "iommu=on"
  ];
  system.stateVersion = "21.05";
}

