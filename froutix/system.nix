{ config, lib, pkgs, ... }:

{
  imports = [
    (import (builtins.fetchGit {
      url = https://framagit.org/bib/dev/nix/check-mk-agent.git;
      rev = "e10cd10dfa2e3690047e3d48346940cf52ac73ae";
    }))
  ];
  services.check-mk-agent.enable = true;

  services.openssh = {
    enable = true;
    passwordAuthentication = true;
    permitRootLogin = "yes";
    ports = [ 22 ];
  };
  systemd.services."online-beep" = {
    description="Beep after all services have started";
    after = [ "network-online.target" "sshd.service" ];
    script = ''
      export PATH=${pkgs.beep}/bin:$PATH
      ${pkgs.kmod}/bin/modprobe pcspkr
      beep --debug -l 50 -f 300
      sleep 0.1
      beep -l 50 -f 200
      beep --debug -l 50 -f 300
    '';
    wantedBy = [ "multi-user.target" ];
    serviceConfig = { Type = "oneshot"; RemainAfterExit = true; };
  };
  i18n.defaultLocale = "en_US.UTF-8";
  console.font = "Lat2-Terminus16";
  console.keyMap = "us";
  time.timeZone = "Europe/Paris";

  environment.systemPackages = with pkgs; [
    # Administrator utilities
    cmatrix molly-guard magic-wormhole
    killall stress-ng speedtest-cli
    vim curl wget file ccze nmap git netcat

    # Certificate management
    acme-sh

    # Networking tools
    iptables tcpdump

    # Hardware monitoring
    lm_sensors smartmontools hddtemp

    # Resource monitoring
    iotop atop htop bmon lsof
  ];

}
