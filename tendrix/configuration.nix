# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./includes/glorytun.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/disk/by-id/ata-Samsung_SSD_840_PRO_Series_S1ANNSAF423101D"; # or "nodev" for efi only

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
  };

  networking.hostName = "tendrix"; # Define your hostname.
  networking.hostId = "1312ACAB";
  networking.useDHCP = false;

  systemd.network =
    let
      tabledInterface = iface: inet: addr: gtway: table: {
        matchConfig = {
          Name = iface;
        };
        linkConfig = {
          RequiredForOnline = false;
          ActivationPolicy = "always-up";
        };
        DHCP = "no";
        extraConfig = "LinkLocalAddressing=no";
        address = [ addr ];
        routes = [ { routeConfig = { Gateway = gtway; Table = table; }; } ];
        routingPolicyRules = [
          {
            routingPolicyRuleConfig = {
              OutgoingInterface = iface;
              Table = table;
            };
          }
          {
            routingPolicyRuleConfig = {
              From = inet;
              Table = table;
            };
          }
          {
            routingPolicyRuleConfig = {
              To = inet;
              Table = table;
            };
          }
        ];
        # dhcpV4Config = {
        #   RouteTable = table;
        # };
        ipv6AcceptRAConfig = {
          RouteTable = table;
        };
      };
    in
      {
        enable = true;
        netdevs = {
          tnd-usr = {
            netdevConfig = { Kind = "vlan"; Name = "tnd-usr";};
            vlanConfig = { Id = 100; };
          };
          uplink-adsl = {
            netdevConfig = { Kind = "vlan"; Name = "uplink-adsl";};
            vlanConfig = { Id = 900; };
          };
          uplink-4g = {
            netdevConfig = { Kind = "vlan"; Name = "uplink-4g";};
            vlanConfig = { Id = 910; };
          };
          mgmt = {
            netdevConfig = { Kind = "vlan"; Name = "mgmt"; };
            vlanConfig = { Id = 999; };
          };

        };
        networks = {
          loopback = {
            matchConfig = { Name = "lo"; };
            address = ["10.12.255.20/32"];
          };
          enp1s0f0 = {
            matchConfig = { Name = "enp1s0f0"; };
            DHCP = "no";
            extraConfig = "LinkLocalAddressing=no";
            vlan = [ "tnd-usr" "uplink-adsl" "uplink-4g" "mgmt" ];
            linkConfig.RequiredForOnline = false;
          };
          mgmt = {
            DHCP = "no";
            extraConfig = "LinkLocalAddressing=no";
            matchConfig = { Name = "mgmt"; };
            address = ["10.12.240.200/24"];
            routes = [ 
              # add routes in uplink tables so that management users can access them
              { routeConfig = { Destination = "10.12.240.200/24"; Scope = "link"; Table = 53; };}
              { routeConfig = { Destination = "10.12.240.200/24"; Scope = "link"; Table = 54; };}
            ];
          };
          tnd-usr = {
            matchConfig = { Name = "tnd-usr"; };
            DHCP = "no";
            address = ["10.12.200.254/24"];
          };
          tun-uplink = {
            matchConfig = { Name = "tun-uplink"; };
            DHCP = "no";
            gateway = [ "10.12.255.255" ];
            dns = [ "80.67.169.12" "80.67.169.40" ];
            extraConfig = "LinkLocalAddressing=no";
            addresses = [ { addressConfig = { Address = "212.83.129.1/32"; Peer = "10.12.255.255/32"; };}];
          };
          uplink-adsl = tabledInterface "uplink-adsl" "192.168.255.0/24" "192.168.255.35/24" "192.168.255.254" 54;
          uplink-4g = tabledInterface "uplink-4g" "192.168.8.0/24" "192.168.8.102/24" "192.168.8.1" 53;
        };
      };

  services.ferm = {
    enable = true;
    config = ''
      table nat {
        chain POSTROUTING {
          saddr 10.12.0.0/16 outerface (uplink-adsl uplink-4g) MASQUERADE;
        }
      }

      table mangle {
        chain POSTROUTING {
          proto tcp tcp-flags (SYN RST) SYN outerface tun-uplink TCPMSS clamp-mss-to-pmtu;
        }
      }
    '';
  };

  networking.glorytun = {
    package = pkgs.callPackage ./packages/glorytun { };
    enable = true;

    interfaces = {
      tun-uplink = {
        keyFile = "/etc/keys/glorytun/uplink.key";
        remoteAddress = "51.159.37.56";
        chacha = true;
        paths = [
          {
            outboundInterfaceName = "uplink-adsl";
            autoRate = true;
            beat = 2;
            rxRate = 20000000; # 20 Mbit/s
            txRate =   800000; # 0.8 Mbit/s
            #lossLimit = 10;
          }
          {
            outboundInterfaceName = "uplink-4g";
            autoRate = true;
            beat = 3;
            rxRate = 24000000; # 24 Mbit/s
            txRate =  8000000; # 8 Mbit/s
            #lossLimit = 20;
          }
        ];
      };
    };
  };

  services.dhcpd4.enable = true;
  services.dhcpd4.interfaces = [ "tnd-usr" ];
  systemd.services."dhcpd4".after = ["systemd-networkd-wait-online.service"];
  services.dhcpd4.extraConfig =
    ''
      option subnet-mask 255.255.255.0;
      option broadcast-address 10.12.200.255;
      option domain-name-servers 80.67.169.12, 80.67.169.40;
      option routers 10.12.200.254;
      subnet 10.12.200.0 netmask 255.255.255.0 {
        range 10.12.200.100 10.12.200.200;
        option interface-mtu 1450;
      }
      host watcher {
        hardware ethernet b8:27:eb:50:70:31;
        fixed-address 10.12.200.201;
      }
      host nport {
        hardware ethernet 00:90:E8:36:50:40;
        fixed-address 10.12.200.202;
      }
    '';


  # Set your time zone.
  time.timeZone = "Europe/Paris";
  services.ntp.enable = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr";
    useXkbConfig = false; # use xkbOptions in tty.
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #   wget
  # ];

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}

