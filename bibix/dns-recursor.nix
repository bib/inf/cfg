{ config, pkgs, lib, ... }:

{
  services.resolved.enable = false;
  services.pdns-recursor = {
    enable = true;
    # Bind on loopbacks
    dns = {
      address = "127.0.0.1 10.13.255.208 ";
      allowFrom = [ "127.0.0.0/8" "10.0.0.0/8" ];
      port = 53;
    };

    # Enable API from localhost
    api = {
      address = "127.0.0.1";
      allowFrom = [ "127.0.0.0/8" ];
      port = 5380;
    };
    settings.api-key = "ion9Yei5eFookamo";

    # Enable DNSSEC processing, export /etc/hosts
    dnssecValidation = "process";
    exportHosts = true;

    # Advanced settings
    settings = {
      tcp-fast-open = 1;
    };
  };
  systemd.services.pdns-recursor.unitConfig.Restart = "no";
  systemd.services.pdns-recursor.restartTriggers = [
    (pkgs.writeText "extra-hosts" config.networking.extraHosts)
  ];
}
