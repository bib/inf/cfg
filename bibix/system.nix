{ config, pkgs, lib, ... }:

{
  imports = [
    (import (builtins.fetchGit {
      url = https://framagit.org/bib/dev/nix/check-mk-agent.git;
      rev = "a5a63b32588669b56cd907799e0e206121cc7bec";
    }))
  ];

  i18n.defaultLocale = "en_US.UTF-8";
  console.keyMap = "us";
  console.font = "Lat2-Terminus16";
  time.timeZone = "Europe/Paris";

  #environment.noXlibs = true;

  services.udisks2.enable = false;
  security.polkit.enable = false;
  security.audit.enable = true;

  # Tweak from https://github.com/lxc/lxd/blob/master/doc/production-setup.md
  security.pam.loginLimits = [
    {
      domain = "*";
      item = "nofile";
      type = "soft";
      value = "1048576";
    }
    {
      domain = "*";
      item = "nofile";
      type = "hard";
      value = "1048576";
    }
    {
      domain = "root";
      item = "nofile";
      type = "soft";
      value = "1048576";
    }
    {
      domain = "root";
      item = "nofile";
      type = "hard";
      value = "1048576";
    }
  ];



  environment.systemPackages = with pkgs; [ 
    # Monitoring tools
    htop atop ssss tmux

    # NixOS image generator
    nixos-generators
  ];

  services.getty.autologinUser = "root";
  services.qemuGuest.enable = true;
  programs.ssh.setXAuthLocation = true;
  services.openssh.enable = true;
  services.openssh.settings.X11Forwarding = true;

  # services.scrutiny.enable = true;
  # services.scrutiny.collector.enable = true;

  programs.mosh.enable = true;
  services.check-mk-agent.enable = true;

  # Monitor network usage, can be disabled afterwards
  services.vnstat.enable = true;

  # Enable mosh, the ssh alternative when client has bad connection
  # Opens UDP ports 60000 ... 61000
  programs.mosh.enable = true;

  system.stateVersion = "19.09";
}
