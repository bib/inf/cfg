{ config, pkgs, lib, ... }:
{
  boot.loader.timeout = 5;
  #boot.loader.timeout = 1;
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda";
  boot.loader.grub.extraConfig = ''
    serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1;
    terminal_input serial;
    terminal_output serial
  '';

  boot.kernelParams = [
    # Limit the ZFS ARC cache size
    "zfs.zfs_arc_max=17179869182"
    # Use serial console
    "console=ttyS0"
  ];

  #boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.kernel.sysctl = {
    # Tweak from https://github.com/lxc/lxd/blob/master/doc/production-setup.md
    "fs.inotify.max_user_watches"   =  524288;   # default:  8192
    "fs.inotify.max_user_instances" =    1024;   # default:   128
    "fs.inotify.max_queued_events"  =   32768;   # default: 16384
    "fs.aio-max-nr"                 =  524288;   # default: 65536
    "kernel.dmesg_restrict"         =       1;   # default:     0
   
    # Allow userland to overcommit memory
    "vm.overcommit_memory" = "1";
  };

  boot.zfs.enableUnstable = true;
  boot.zfs.extraPools = [
    "bibix-cold"
  ];
}
