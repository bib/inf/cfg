{ config, pkgs, lib, ... }:
with lib; with strings;

let

  # Function definitions
  # ===================================================================================================================

  # Container interfaces
  # -------------------------------------------------------------------------------------------------------------------

  containerInterfaces = mapAttrs' (name: address: { name = "lxd-${name}"; value = {
    name = "lxd-${name}";
    addresses = [ { addressConfig = {
      Address = "${config.deployment.internalIPv4}/32"; Peer = "${address}/32";
    };} ];
    linkConfig = { RequiredForOnline = "no"; };
  }; }) config.deployment.containerMap;

  # Container hosts
  # -------------------------------------------------------------------------------------------------------------------

  serviceHost = name: address: "${address} ${name}.lebib.lan";
  serviceHosts = concatStringsSep "\n" (mapAttrsToList serviceHost config.deployment.containerMap);

in

{

  # Configuration values
  # ===================================================================================================================

  # Generic networking settings
  # -------------------------------------------------------------------------------------------------------------------

  networking = {
    hostName = "bibix";
    hostId = "c29fb78a";
    useDHCP = false;
    firewall.enable = false;
    wireguard.enable = true;
    nameservers = [ "127.0.0.1" ];
    search = [ "lebib.org" ];
    extraHosts = ''
      ${serviceHosts}
      10.14.255.255 roko.pstch.net roko
      10.14.255.224 notix.pstch.net notix
      10.13.255.208 bibix.lebib.lan bibix
      10.13.159.254 mail.lebib.lan
    '';
    iproute2.rttablesExtraConfig = ''
      13208 uplink
    '';
  };
  systemd.network.enable = true;

  # Virtual networks
  # -------------------------------------------------------------------------------------------------------------------

  systemd.network.netdevs = {
    # Uplink tunnel to `roko.pstch.net`
    tun-uplink = {
      netdevConfig = {
        Kind = "wireguard";
        Name = "tun-uplink";
      };
      wireguardConfig = {
        PrivateKeyFile = "/etc/keys/wireguard/tun-uplink/bibix.priv";
        FirewallMark = 13208;
        ListenPort = 53208;
      };
      wireguardPeers = [
        { wireguardPeerConfig = {
            Endpoint = "51.159.37.56:13208";
            AllowedIPs = [
              "0.0.0.0/0"     # allow using as default gateway
              "224.0.0.0/8"   # allow OSPF traffic
            ];
            PublicKey = "M7CyMAC0e7PhaemR2NkXuqDuVMPGuWEER9Vjz/gzvHg=";
            PresharedKeyFile = "/etc/keys/wireguard/tun-uplink/shared.key";
            PersistentKeepalive = 5;
        }; }
      ];
    };
    # Uplink tunnel to `roko.pstch.net`
    tun-malaka = {
      netdevConfig = {
        Kind = "wireguard";
        Name = "tun-malaka";
      };
      wireguardConfig = {
        PrivateKeyFile = "/etc/keys/wireguard/tun-malaka/bibix.priv";
        ListenPort = 53236;
      };
      wireguardPeers = [
        { wireguardPeerConfig = {
            Endpoint = "109.197.105.130:13236";
            AllowedIPs = [
              "10.13.159.0/24"
              "10.13.255.236/32"
            ];
            PublicKey = "uJ0Gu1cykHLDc4bA7AMzOz46VUKu/Rzu4QuFS6v6+SA=";
            PresharedKeyFile = "/etc/keys/wireguard/tun-malaka/shared.key";
            PersistentKeepalive = 5;
        }; }
      ];
    };
    # Management tunnel
    tun-mgmt = {
      netdevConfig = {
        Kind = "wireguard";
        Name = "tun-mgmt";
      };
      wireguardConfig = {
        PrivateKeyFile = "/etc/keys/wireguard/tun-mgmt/bibix.priv";
        ListenPort = 53248;
        FirewallMark = 13208;
      };
      wireguardPeers = [
	# @OBB (old brokix bastard)
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.203/32" "10.13.252.203/32" ];
          PublicKey = "aZ6jBXJzud07qW/+AAt9NT4VzzTiyqIXhQZrBOlgtHA=";
          PersistentKeepalive = 30;
        }; }

        # @pstch (pix)
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.1/32" ];
          PublicKey = "GJddzSoStIh1qbK21Y+Nr8gii+3U+ofygdRmfWEjN0U=";
          PersistentKeepalive = 30;
        }; }
        # @smyds
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.2/32" ];
          PublicKey = "5uXduzAlHSiVvmWLfcycDQY/t+PJupPQcnBOju/q8xM=";
          PersistentKeepalive = 30;
        }; }
        # @wargreen (fetide)
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.3/32" ];
          PublicKey = "DMQ1UT+ZeOXsJ5cWvOoIT66Fvgu6B7byCCRpfHXcQxQ=";
          PersistentKeepalive = 30;
        }; }
        # @wargreen (lachoze)
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.4/32" "192.168.255.0/24" ];
          PublicKey = "t4Qy26H9MuPRk+Q4IlSHxBMlmcskBweQuBwwUb197BQ=";
          PersistentKeepalive = 30;
          }; }
        # @vonhammer
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.5/32" ];
          PublicKey = "QvexHDicB1S/BNRKiscKbaDC34fs1d+0as4EpmKV3iI=";
          PersistentKeepalive = 30;
        }; }
        # @benbart (thinkette)
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.6/32" ];
          PublicKey = "/sd3m562O8YmfeOlza79FJasDIr6YBglbTtn1IKG2Fw=";
          PersistentKeepalive = 30;
        }; }
        # @benbart (ploper)
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.7/32" "10.17.0.0/16" ];
          PublicKey = "fEAXZwpihoXQAc8oni43uSew9ivtNXRM3ggI1Z+X3FE=";
          PersistentKeepalive = 30;
        }; }
        # froutix.lebib.org
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.255.202/32" "10.13.255.221/32" "10.13.255.222/32" "10.13.221.0/24" "10.13.222.0/24" ];
          PublicKey = "AAnaJ378E0nxQ8/wNxN8WZChUNCrF26YKKf/ScALpDE=";
          PersistentKeepalive = 30;
        }; }
        # @luc (debian)
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.9/32" ];
          PublicKey = "UE47y/j7FYaaE+DGxAt2sWRfaBYVSXB6JoxVTrsl/Wk=";
          PersistentKeepalive = 30;
        }; }
        # @mare (Bip)
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.10/32" ];
          PublicKey = "PIScOiekuPucZTQPPD49VA7nfMUhLgqZxwTMBdDqREA=";
          PersistentKeepalive = 30;
        }; }
        # @martial
        { wireguardPeerConfig = {
          AllowedIPs = [ "10.13.248.11/32" ];
          PublicKey = "FGsMle1B8iCqvhWXdsDi1cVDcdkiMWBAQ1lUlp7181c=";
          PersistentKeepalive = 30;
        }; }
      ];
    };
  };

  # Iodine tunnel (IP over DNS)
  # -------------------------------------------------------------------------------------------------------------------

  # Interface definitions
  # -------------------------------------------------------------------------------------------------------------------

  # systemd.services.systemd-networkd.environment.SYSTEMD_LOG_LEVEL = "debug";

  systemd.network.networks = {
    # Loopback interface
    loopback = {
      name = "lo";
      address = [ "127.0.0.1/8" "10.13.255.208/32" ];
      extraConfig = ''
        [RoutingPolicyRule]
        FirewallMark=13208
        Table=13208
      '';
    };

    # Uplink bridge to `notix.pstch.net`
    enp0s3 = {
      name = "enp0s3";
      addresses = [
        { addressConfig = {
          Address = "10.13.255.208/32"; Peer = "10.14.255.224/32"; }; }
        { addressConfig = {
          Address = "163.172.231.232/32"; }; }
      ];
      routes = [
        { routeConfig = {
          Destination = "0.0.0.0/0"; Gateway = "10.14.255.224"; Table = 13208; }; }
        { routeConfig = {
          Destination = "10.14.255.224"; Scope = "link"; Table = 13208; }; }
      ];
    };

    # Uplink tunnel to `roko.pstch.net`
    tun-uplink = {
      name = "tun-uplink";
      addresses = [
        { addressConfig = {
          Address = "163.172.231.232/32"; Peer = "10.14.255.255/32"; }; }
      ];
      routes = [
        { routeConfig = {
          Destination = "0.0.0.0/0"; Gateway = "10.14.255.255"; }; }
      ];
    };
    tun-malaka = {
      name = "tun-malaka";
      linkConfig = { RequiredForOnline = "no"; };
      addresses = [
        { addressConfig = {
          Address = "10.13.255.208/32"; Peer = "10.13.255.236/32"; }; }
      ];
      routes = [
        { routeConfig = {
          Destination = "10.14.0.0/15"; Gateway = "10.14.255.224"; }; }
        { routeConfig = { 
          Destination = "10.13.255.236/32"; Scope = "link"; }; }
        { routeConfig = {
          Destination = "10.13.159.0/24"; Gateway = "10.13.255.236"; }; }
      ];
    };
    tun-mgmt = {
      name = "tun-mgmt";
      addresses = [
        { addressConfig = { Address = "10.13.255.208/32"; }; }
      ];
      routes = [
        { routeConfig = { Destination = "10.13.248.0/24"; Scope = "link"; }; }
        { routeConfig = { Destination = "192.168.255.0/24"; Scope = "link"; }; }
        { routeConfig = { Destination = "10.13.252.203/32"; Gateway = "10.13.248.203"; }; }
        { routeConfig = { Destination = "10.13.255.202/32"; Scope = "link"; }; }
        { routeConfig = { Destination = "10.13.255.221/32"; Scope = "link"; }; }
        { routeConfig = { Destination = "10.13.255.222/32"; Scope = "link"; }; }
        { routeConfig = { Destination = "10.13.221.0/24"; Scope = "link"; }; }
        { routeConfig = { Destination = "10.13.222.0/24"; Scope = "link"; }; }
        { routeConfig = { Destination = "10.17.0.0/16"; Scope = "link"; }; }
      ];
    };


  } // containerInterfaces;

  # Traffic control
  # -------------------------------------------------------------------------------------------------------------------

  systemd.services."tc-tun-uplink" = {
    requires = [ "sys-subsystem-net-devices-tun\\x2duplink.device" ];
    after = [ "sys-subsystem-net-devices-tun\\x2duplink.device" ];
    wantedBy = [ "network-online.target" ];
    serviceConfig = { Type = "oneshot"; RemainAfterExit = true; };
    script = ''
      ${pkgs.iproute}/bin/tc qdisc del dev tun-uplink root 2>/dev/null || true
      ${pkgs.iproute}/bin/tc qdisc add dev tun-uplink root handle 1:0 htb default 1
      ${pkgs.iproute}/bin/tc class add dev tun-uplink parent 1:0 classid 1:1 htb rate 200mbit burst 20mbit
      ${pkgs.iproute}/bin/tc qdisc add dev tun-uplink parent 1:1 handle 2: fq_codel
    '';
  };

  systemd.services."init-netfilter-tables" = {
    wantedBy = [ "network-online.target" ];
    serviceConfig = { Type = "oneshot"; RemainAfterExit = true; };
    script = ''
      ${pkgs.iptables}/bin/iptables -L
      ${pkgs.iptables}/bin/ip6tables -L
    '';
  };

}
