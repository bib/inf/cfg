{ config, pkgs, lib, ... }:

with lib;

let
  ssh_keys = {};
in

{
  security.sudo.wheelNeedsPassword = false;

  users.motd = ''
+---------------+--------------------------------------------------------------+
|  NixOS 19.09  |  bibix.lebib.org                                             |
|   \\  \\ //   |    Admins      : pistache, smyds, wargreen                   |
|  ==\\__\\/ // |    Services    : LXD host, WireGuard, BIRD, haproxy          |
|    //   \\//  |    Router-ID   : 10.14.224.9                                 |
| ==//     //== |    CPU         : 2 vCPU cores (Intel i7-7700K @ 4.200GHz)    |
|  //\\___//    |    Memory      : 4 GiB (+ 8 GiB SWAP)                        |
| // /\\  \\==  |    Storage     : main:40GB/SSD / hot:40GB/SSD / cold:1TB/HDD |
|   // \\  \\   |                                                              |
+---------------+--------------------------------------------------------------+
### NOTES: ###
 - Channels are back in use, as the patches that were required by lxd-containers
   have been made optional, and updates are cumbersome without channels
   (we'd have to find the latest working nixpkgs checkout at each upgrade)
 - This machine is a work-in-progress, host configuration may change.
  '';

  users.users = (mapAttrs (name: admin: {
    isNormalUser = true;
    extraGroups = [ "wheel" "lxd" ];
    openssh.authorizedKeys.keys = admin.ssh_keys;
  }) config.deployment.admins) // {
    root.openssh.authorizedKeys.keys = flatten (mapAttrsToList (name: admin:
      map (line: "${line} (${name})") admin.ssh_keys
    ) config.deployment.admins);
  };
}
