{ config, lib, ... }:

{
  services.sanoid = let syncPlan = {
    recursive = true;
    daily = 7;
    hourly = 24;
    monthly = 12;
    yearly = 1;
  }; in {
    enable = true;
    extraArgs = [ "--verbose" ];
    datasets."bibix-main/LXD/containers" = syncPlan;
    datasets."bibix-cold/BCK/containers" = syncPlan // {
      autosnap = false;
      autoprune = true;
    };
    datasets."bibix-main/LXD/custom" = syncPlan;
    datasets."bibix-cold/BCK/custom" = syncPlan // {
      autosnap = false;
      autoprune = true;
    };
    datasets."bibix-cold/LXD/custom" = syncPlan;
  };
  services.syncoid = {
    enable = true;
    commonArgs = [ "--no-sync-snap" ];
    commands."bibix-main/LXD/containers" = {
      target = "bibix-cold/BCK/containers";
      recursive = true;
    }; 
    commands."bibix-main/LXD/custom" = {
      target = "bibix-cold/BCK/custom";
      recursive = true;
    };
  };
}
