{ config, pkgs, lib, ... }:

{
  imports = [
    # LXD containers module
    #(import (builtins.fetchGit {
    #  url = https://framagit.org/bib/dev/nix/lxd-containers.git;
    #  rev = "a1c356058f81ecea2133152b3026d52a1e8d1280";
    #}))
  ];

  users.users.lxd.subUidRanges = [ { startUid = 1000000; count = 10000000000; } ];
  users.users.lxd.group = "lxd";
  users.users.root.subUidRanges = lib.mkForce config.users.users.lxd.subUidRanges;

  users.users.lxd.isSystemUser = true;

  virtualisation.lxc.lxcfs.enable = true;
  virtualisation.lxd = {
    enable = true;
    zfsSupport = true;

    #projects.default.containers = {
    #  base-nixos.image = {
    #    type = "nixos";
    #    nixosConfig = { config, ...}: {
    #
    #    };
    #  };
    #};
  };
}
