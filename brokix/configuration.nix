# Ce fichier est le fichier de configuration principal de 
# `brokix.lebib.org`. L'intégralité de la configuration déclarée de la 
# machine est soit décrite ici, soit décrite dans un fichier lié par 
# celui-ci. Certaines parties de la configuration peuvent avoir été 
# réalisées de manière impérative et sont quand même persistées, parce que 
# tout faire de manière déclarative ça prend du temps, et que des fois on 
# prend des raccourcis.

{ config, lib, pkgs, ... }:

{
  # On importe le fichier ./hardware-configuration.nix, qui a été généré 
  # par l'installeur de NixOS (nixos-generate-config) lors de la mise en 
  # place initiale de la machine, après que tous les systèmes de fichiers 
  # nécessaires au démarrage de la machine aient été montés. Ce fichier n'a 
  # pas a être modifié, il sera potentiellement regénéré (a la main) lors 
  # de certaines mises a jour.
  imports =
    [ 
      ./hardware-configuration.nix
    ];

  # On utilise systemd-boot comme chargeur de démarrage : c'est un programme lancé
  # par le BIOS de la machine, et qui va ensuite charger Linux. On utilise 
  # systemd-boot plutot que GRUB, qu'on retrouve souvent sur nos machines, car
  # systemd-boot est plus simple et gère un peu mieux les consoles séries.
  boot.loader.systemd-boot.enable = true;

  # Ce paramètre définit les options qui vont être passées au noyau Linux par
  # systemd-boot.
  boot.kernelParams = [ 
    # On active deux consoles : une sur l'écran normal du serveur (port VGA), et 
    # une sur le port série qui est redirigée ensuite vers l'IPMI, ce qui permet
    # d'accéder a la console du serveur a distance. Par défaut, seule la console
    # série permet d'utiliser le clavier, mais on peut changer ça pendant le
    # démarrage en inversant les deux consoles depuis systemd-boot.
    "console=tty0"               # Show passive console on VGA
    "console=ttyS1,115200n8"     # Show active console on COM2

    # On active des options de "hardening", rendant le noyau plus résistant aux 
    # tentatives d'exploitation (par exemple si une application web est compromise,
    # le noyau doit pouvoir résister aux tentatives de l'attaquant.e de casser
    # l'isolation qu'on a mis en place et qui protégerait le reste de l'infra.
    # Les commentaires (en "anglais") sont issus de la documentation de Tails.
    "init_on_free=1"             # Fill freed pages and heap objects with zeroes.
    "slab_nomerge"               # Disables the merging of slabs of similar sizes
    "page_alloc.shuffle=1"       # Enables page allocator freelist randomization.
    "kernel.kptr_restrict=2"     # Hide /proc/kallsyms
    "mds=full,nosmt"             # Fully enable MDS mitigations
    "randomize_kstack_offset=on" # Randomize kernel stack offset on syscall entry

    # ACAB
    "acab"                       # NonobstantAlways
  ];

  # Le démarrage d'un système GNU/Linux se passe souvent en deux étapes :
  # en premier lieu, on charge un petit système en RAM, qu'on appelle 
  # "initramfs" ou "initrd", qui contient ce qu'il faut pour charger le 
  # reste du stockage, et lancer "systemd" qui va démarrer les programmes
  # nécessaires au fonctionnement des services de la machine. Dans notre 
  # cas, c'est utile car c'est ce petit système indépendant qui va s'occuper
  # de déchiffrer les disques (et donc de nous demander la phrase de passe)
  #
  # Traditionellement, ce petit système fonctionne grace a un ensemble
  # de scripts "bash" assez fragiles et difficiles a modifier. NixOS
  # nous permet d'utiliser "systemd" dans l'initrd lui-même plutot que
  # cet ensemble de script, et c'est avec cette option qu'on demande 
  # d'utiliser "systemd" dans ce petit système de lancement.
  #
  # Pour plus d'informations sur ce sujet ("initrd" et processus de lancement
  # de la machine, il y a la page de manuel "bootup" (man bootup) qui peut
  # être très utile.
  boot.initrd.systemd.enable = true;

  # Afin d'avoir le plus de déterminisme et de reproductibilité dans
  # l'état de la configuration du système, et afin de s'assurer que seules
  # les informations que nous souhaitons sont persistées sur les disques,
  # on fait en sorte que le système de fichiers "racine" soit stocké dans la
  # RAM. NixOS nous permet de faire ça, car les programmes et librairies
  # nécessaires sont stockées dans une partition a part qui a des règles
  # de fonctionnement particulières (tous les paquets sont dans des dossiers
  # séparés et identifiés par un hash unique, et ne sont pas modifiables). 
  #
  # Pour que le système puisse quand même fonctionner, cette configuration
  # va aussi s'occuper de faire en sorte que certaines données systèmes soit
  # quand même persistentes, mais de manière chiffrée :
  #  - le store Nix, dans /nix/store
  #  - la base de données de Nix, qui lui sert a suivre les dépendances
  #    entre les paquets installés, dans /nix/var/nix
  #  - les clefs de chiffrement (pour les disques et les tunnels, 
  #    notamment), dans /etc/keys
  #  - les répertoires personels des admins de la machine, dans /home
  #  - le stockage des données persistentes des conteneurs
  #
  # Tout le reste est défini explicitement dans la configuration. 
  # Cette option dit a NixOS d'utiliser un "tmpfs" pour "/" (la racine),
  # et implique que tout ce qui doit être persisté doit être déclaré 
  # dans la configuration du système.
  fileSystems."/" = {
    fsType = "tmpfs";
  };

  # Configuration du réseau
  # ==========================================================================
  # Cette section configure les paramètres réseau de cette machine.
  
  networking = {
    # On définit le nom d'hôte de cette machine, ainsi qu'un identifiant 
    # hexadécimal choisi aléatoirement a l'installation.
    hostName = "brokix";
    hostId = "abbc7635";

    # On désactive le client DHCP intégré car nous allons utiliser 
    # systemd-networkd pour configurer le réseau de manière déclarative.
    useDHCP = false;

    # On désactive le pare-feu intégré a NixOScar nous allons utiliser
    # nftables pour configurer le pare-feu de manière déclarative.
    firewall = false;

    # On active wireguard, car on va l'utiliser pour nos tunnels,
    # en le configurant au travers de systemd-networkd.    
    wireguard.enable = true;
    
    # Pour l'instant, on utilise les serveurs DNS de FDN, plus tard, il y a
    # aura un résolveur local sur cette machine.
    nameservers = [
      # Adresses IPv4 des résolveurs DNS de FDN      
      "80.67.169.12" 
      "80.67.169.40" 
      # Adresses IPv6 des résolveurs DNS de FDN
      "2001:910:800::12"
      "2001:910:800::40"
    ]

    # On définit lebib.org comme "domaine de recherche", ce qui fait que si 
    # on essaie de résoudre un nom non qualifié (disons "bibix"), le 
    # système essaiera de résoudre "bibix.lebib.org". C'est pratique (mais 
    # pas essentiel).
    search = [ "lebib.org" ];

    # Cette option permet de rajouter des noms d'hôtes (utilisés pendant la
    # la résolution de nom) de manière statique dans cette machine.
    extraHosts = ''
    '';
  };
 
  # On active un composant de systemd nommé `systemd-networkd`, qui permet 
  # de gérer le réseau de manière déclarative : on décrit notre 
  # configuration réseau dans un ensemble de fichiers (qui ressemblent aux 
  # fichiers utilisés pour configurer les services du système), et le 
  # processus `systemd-networkd` s'occupe d'appliquer cette configuration. 
  # Ca permet d'éviter tout un tas de script un peu fragiles de 
  # configuration du réseau, au prix d'une configuration du réseau un peu 
  # plus verbeuse. C'est un choix plutôt adapté pour cette machine, ou la 
  # configuration réseau doit être fiable mais ne changera pas souvent.
  systemd.network.enable = true;

  # On configure ici un périphérique réseau virtuel ("netdev" dans la langue
  # de systemd, voir `man systemd.netdev`) appelé `tun-uplink` (pour "tunnel
  # uplink", "uplink" se traduisant par "lien montant"), et qui permettra a
  # `brokix` d'être relié au reste du réseau du BIB, et d'être accessible par
  # une adresse IP publique dédiée. 
  #
  # On a besoin de faire ça car on souhaite faire de l'auto-hébergement, mais
  # que nous n'avons pas encore de réseau IP sur lequel se placer, pour pouvoir
  # donner des adresses publiques nous avons donc deux choix :
  #  - rediriger des ports sur chaque réseau ou nos machines sont hébergées,
  #    dans les FreeBox/LiveBox/etc.
  #    + c'est ennuyant car ça nous empêche de déplacer physiquement les machines
  #      sans changer leur adresse IP publique
  #    + c'est aussi difficile de garantir que les fournisseurs grand public
  #      préservent la même adresse publique
  #    + dans les zones denses, ce n'est même pas possible car les fournisseurs
  #      nous font partager notre IP avec d'autres abonnés, ce qui nous empêche
  #      de rediriger les ports nécessaires
  #  - connecter chaque machine a un VPN avec une adresse IP statique : on a choisi
  #    cette solution, mais plutot que de louer un VPN pour chaque machine, on loue
  #    un serveur virtuel, qui sert de "point de sortie" pour différentes machines,
  #    sur lequel on configure des tunnels Wireguard
  #    + ça nous permet d'assigner des adresses IP uniques pour chaque machine (elles
  #      coutent 2E/mois, en plus du serveur qui coute autour de 20E/mois)
  #    + tout le traffic entre ces différentes machines peut être routé directement
  #      au niveau du point de sortie
  #
  # Donc, pour revenir a notre configuration, ici nous configurons le coté "brokix"
  # de ce tunnel, représenté par un "netdev" systemd dont le nom est "tun-uplink".
  systemd.network.netdevs.tun-uplink = {
    # On indique a `systemd-networkd` le nom de notre tunnel, et le fait que c'est un
    # tunnel Wireguard
    netdevConfig = {
      Kind = "wireguard";
      Name = "tun-uplink";
    };
    wireguardConfig = {
      # On donne le chemin vers la clef privée de `brokix` sur ce tunnel, qui 
      # servira a déchiffrer les communications venant du serveur, qui auront été
      # chiffrées avec la clef publique associée, présente dans la configuration
      # du serveur.
      PrivateKeyFile = "/etc/keys/wireguard/tun-uplink/brokix.priv";
      # On donne le numéro de port sur lequel on écoute, en redirigeant ce port
      # sur le réseau qui héberge `brokix`, cela permet de récupérer des coupures
      # plus rapidement, car sinon le serveur doit attendre que `brokix` lui parle
      # pour connaitre son IP. Spécifier le port de manière statique est aussi utile
      # pour autoriser le tunnel dans notre pare-feu.
      ListenPort = 53203;
      # On rajoute un "tag" (13255) sur les paquets du tunnel, afin de pouvoir indiquer
      # dans le pare-feu que seuls les paquets du tunnel lui-même peuvent être envoyés
      # dans le réseau qui héberge la machine : tout le reste du traffic vers Internet,
      # ou vers le reste du BIB, devra passer dans le tunnel.
      #
      # Cette option permet d'éviter les "hacks" habituels a base de routes `/1` utilisés
      # notamment par OpenVPN, et permet de mettre en place du source-based-routing : on 
      # route les paquets différemment selon que ce soit des paquets intérieurs ou 
      # extérieurs au VPN. On retrouve ce "tag" (13255) dans les règles de routage de 
      # la machine, un peu plus bas.
      FirewallMark = 13255;

    };
    wireguardPeers = [
      # On configure maintenant les "pairs" de ce tunnel, dans notre cas il n'y en a qu'un seul,
      # qui est `roko`, un serveur loué chez Online, a Paris, sur lequel nous faisons arriver
      # les tunnels de nos machines, comme expliqué plus haut. 
      { wireguardPeerConfig = {
          # On indique a quelle adresse IP (et sur quel port) se connecter pour mettre en place
          # ce tunnel, c'est l'adresse IP de `roko.lebib.org` qui est indiquée ici.
          Endpoint = "51.159.37.56:13208";
          # On indique que ce pair peut nous transmettre des paquets issus de n'importe quelle
          # adresse IP.
          AllowedIPs = [ "0.0.0.0/0" ];
          # On indique la clef publique du serveur, qui servira a chiffrer les paquets 
          # envoyés au serveur, qui pourra les déchiffrer avec sa clef publique associée.
          PublicKey = "TODO";
          # On ajoute aussi une clef partagée (symmétrique), qui doit être la même du coté de `roko`.
          # Cette couche cryptographique supplémentaire permet d'éviter de potentielles attaques
          # d'ordinateur quantiques, en faisant en sorte de ne pas nous baser uniquement sur de 
          # la cryptographie assymétrique (vulnérable aux ordinateurs quantiques), et nous coute 
          # pratiquement rien a l'usage.
          PresharedKeyFile = "/etc/keys/wireguard/tun-uplink/shared.key";
          # On indique qu'il faut tenter de communiquer avec le serveur au moins toutes les cinq
          # secondes.
          PersistentKeepalive = 5;
      }; }
    ];
  };

  # On vient de configurer la partie "lien" des tunnels, il nous faut maintenant
  # déclarer la configuration "réseau" (c'est a dire, les adresses IPs, les routes, etc),
  # de différentes interfaces réseau de la machine.
  systemd.network.networks = {
    # On configure tout d'abord l'interface "boucle" de la machine, c'est une interface
    # réseau un peu speciale qui permet a la machine de se "parler a elle-même". Cette
    # interface réseau est toujours fonctionelle, on peut donc y assigner les adresses 
    # IP spécifiques a cette machine, et y indiquer de la configuration réseau qui n'est
    # pas spécifique une interface
    loopback = {
      name = "lo";
      address = [ 
        # Le réseau `127.0.0.1/8` est universellement un réseau "loopback", qui permet
        # de s'adresser a la machine locale.
        "127.0.0.1/8" 
        # On indique ici l'adresse IPv4 privée de cette machine. Cette adresse sera 
        # configurée sur toutes les autres interfaces, mais l'avoir ici permet d'être
        # sure qu'elle soit assignée, indépendamment de l'état des autres interfaces.
        "10.13.255.208/32" 
        # Pour la même raison, on indique aussi ici l'adresse IPv4 publique de cette 
        # machine. Bien que cette machine soit dans un réseau privé, l'usage d'un tunnel
        # vers le point de sortie, a nous permet de rediriger une des adresses IPs liées 
        # au point de de sortie vers `brokix`, a travers le tunnel, en utilisant une technique
        # appelée "proxy ARP", mise en place au niveau du point de sortie, et expliquée
        # dans sa documentation.
        "X.X.X.X/32"
        # TODO: Ajouter l'IPv6 publique de cette machine
        ];
      # Ensuite, on indique au système de routage de Linux que les paquets portant le 
      # "tag" 13255 (appliqué aux paquets extérieurs du tunnel, plus haut, dans la
      # configuration du tunnel) doivent être routés par une table de routage spécifique
      # nommée `13255`, comme le tag, plutot que par la table de routage par défaut
      extraConfig = ''
        [RoutingPolicyRule]
        FirewallMark=13255
        Table=13255
      '';
    };

    # Ici, on configure l'interface réseau connectée au réseau physique dans lequel la
    # machine est installée. Pour l'instant, et par défaut, on se configure en DHCP,
    # si besoin on se mettra en statique pour pouvoir rediriger des ports.
    #
    # On se branche a Internet par le port physique 1, a l'arrière de la machine, qui 
    # est représenté sur la machine par l'interface `eno1`.
    eno1 = {
      name = "eno1";
      addresses = [
        # On indique ici l'adresse IP que `brokix` doit utiliser sur le réseau local,
        # pour communiquer avec la "box" qui lui fournit Internet.
        { addressConfig = { Address = "192.168.0.203/24"; }; }
      ];
      # Maintenant, on configure des routes pour faire passer le traffic de cette machine
      # par la passerelle (c'est a dire la "box" qui fournit Internet), mais **uniquement**
      # pour le traffic de la table de routage `13255`, qui est le traffic portant le tag
      # `13255` : les paquets extérieurs de notre tunnel `tun-uplink`.
      #
      # C'est ce qui nous permet de rediriger "tout" le traffic par le tunnel, mais en
      # excluant les paquets du tunnel lui-même. Faire passer le tunnel par le tunnel,
      # c'est pas du réseau, c'est une bouteille de Klein !
      routes = [
        # On indique ici que tout le traffic de la table `13255` a destination 
        # de `0.0.0.0/0` (c'est a dire, tout Internet) doit passer par l'adresse
        # indiquée dans `Gateway`, qui doit être l'adresse du routeur local (FreeBox,
        # LiveBox, etc...)
        { routeConfig = {
          Destination = "0.0.0.0/0"; Gateway = "192.168.0.255"; Table = 13255; }; }
        # Et maintenant, on indique que tout le traffic de la table `13255` a destination
        # de `192.168.0.0/24` (c'est a dire, le réseau local) doit être envoyé par
        # l'interface réseau actuelle. On appelle ça une route de niveau "lien", elle
        # serait configurée automatiquement par le fait qu'on ait une adresse sur ce
        # réseau, mais étant donné que nous sommes dans une table de routage spécifique
        # nous devons la configurer nous-mêmes.
        { routeConfig = {
          Destination = "192.168.0.0/24"; Scope = "link"; Table = 13255; }; }
      ];
    };

    # Et finalement, on configure les adresses IPs et les routes de notre tunnel,
    # sur l'interface créée par Wireguard a partir de la configuration du tunnel
    # lui-même. 
    tun-uplink = {
      name = "tun-uplink";
      # On indique les adresses IPs qui doivent être assignées a cette interface
      addresses = [
        # Ici, on spécifie l'adresse IP publique de notre machine, qui est aussi
        # notre adresse de coté du tunnel (on évite ainsi d'avoir pleins d'adresses
        # IP différentes pour chaque machine). Pour faire ça, ce tunnel est configuré
        # en "lien point-a-point", c'est a dire que plutot que d'indiquer une adresse 
        # dans un sous-réseau particulier, on assigne notre adresse en spécifiant
        # l'adresse IP utilisée de l'autre coté du tunnel (l'adresse IP interne
        # de `roko`, dans notre cas).
        { addressConfig = {
          Address = "163.172.231.232/32"; Peer = "10.14.255.255/32"; }; }
      ];
      # On indique les routes IPs qui doivent être spécifiques a cette interface
      routes = [
        # Et ici, on indique que tout le traffic de la machine doit être routé
        # par `roko`, en indiquant que le traffic vers `0.0.0.0/0` (tout Internet)
        # doit être routé par `10.14.255.255` (l'adresse IP interne de `roko`)
        { routeConfig = {
          Destination = "0.0.0.0/0"; Gateway = "10.14.255.255"; }; }
      ];
    };
  };

  # Configuration du système
  # ==========================================================================
  # Cette section configure des paramètres un peu "généraux" de cette machine,
  # qui ne rentrent pas dans une section particulière.

  # On définit le fuseau horaire utilisé pour représenter les dates sur 
  # cette machine.
  time.timeZone = "Europe/Paris";

  # Cette option expose le fichier de configuration actuellement chargé
  # dans le dossier /run, ce qui permet d'exposer facilement la configuration
  # actuelle. Ca peut être pratique quand on a des problèmes a l'activation
  # d'une configuration, ou qu'on perd l'accès au fichier de configuration,
  # ou pour pouvoir exposer publiquement la configuration actuellement en place.
  system.copySystemConfiguration = true;

  # On définit des programmes installés de base sur la machine, c'est
  # pratique parce que du coup on a pas besoin de faire "nix-shell -p"
  # a chaque fois qu'on utilise un programme
  environment.systemPackages = with pkgs; [
    # Un client IPMI qui permet d'interagir, par le protocole IPMI, avec
    # le "BMC", un petit ordinateur dans la machine qui permet de le 
    # controler a distance (allumer/éteindre, surveiller la conso, les
    # ventilateurs, les alims, et d'accéder a une console série a distance.
    ipmitool
    # Un petit éditeur de texte sympa (pour quitter c'est "killall vim" 
    # depuis un autre terminal.)
    vim
    # Un programme qui permet de télécharger des fichiers par le protocole
    # HTTP
    wget
    # 
  ];

  # Certains programmes ne peuvent pas être installés par systemPackages 
  # (l'option précédente), car ces programmes ont besoin de permissions
  # supplémentaires ("bit setuid", ce genre de trucs). Cette option
  # permet aussi de spécifier des configurations particulières pour ces
  # programmes installés.
  programs.mtr.enable = true;

  # On active un serveur SSH qui permet d'acceder a la machine a distance
  # avec une interface texte.
  services.openssh.enable = true;

  # Configuration du comptes utilisateurs
  # ==========================================================================
  # Cette section configure les comptes utilisés pour accèder a cette machine
  # et pour l'administrer, ainsi que des options de sécurité.

  # On définit un compte d'administration avec mot de passe initial. C'est
  # temporaire, le temps qu'on détermine comment on gère les comptes de nos
  # serveurs.
  users.users.root.initialPassword = "dontpanic";

  # On définit quelques comptes d'administration
  # TODO: Ajouter un stockage persistent pour les mots de passe
  # TODO: Ajouter de quoi charger les comptes et clefs SSH depuis 
  #       l'extérieur de la la configuration.
  users.users.pistache = {
    # Cette option indique que ce compte sera utilisée par une personne,
    # et n'est pas un compte "système".
    isNormalUser = true;
    # Cette option ajoute l'utilisateur au groupe "wheel", ce qui permet
    # d'utiliser "sudo" pour devenir root et avoir tous les droits sur la
    # machine.
    extraGroups = [ "wheel" ];
  };

  # On fait en sorte que les comptes utilisateur dans le groupe "wheel" 
  # n'aient pas besoin de mot de passe pour utiliser "sudo", car cela 
  # n'apporte pas de sécurité supplémentaire, et permet aux admins de ne 
  # pas avoir de mot de passe sur cette machine en utilisant 
  # l'authentification par clef SSH.
  security.sudo.wheelNeedsPassword = false;

  # Cette option renforce la sécurité de "sudo" en faisant en sorte que 
  # "sudo" ne soit utilisable que par les membres du groupe "wheel".
  security.sudo.execWheelOnly = true;

  # Configuration interne à Nix
  # ==========================================================================

  # Cette option est interne a NixOS, et ne doit surtout pas être modifiée,
  # MEME EN CAS DE MISE A JOUR DU SYSTEME D'EXPLOITATION, car elle sert a
  # définir la version de NixOS qui était présente au moment de l'installation.
  system.stateVersion = "23.11"; 
}
