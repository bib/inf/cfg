{ config, lib, ... }:

{
  services.sanoid = let syncPlan = {
    recursive = true;
    daily = 7;
    hourly = 24;
    monthly = 12;
    yearly = 1;
  }; in {
    enable = true;
    extraArgs = [ "--verbose" ];
    datasets."storix-main/LXD/containers" = syncPlan;
    datasets."storix-cold/BCK/containers" = syncPlan // {
      autosnap = false;
      autoprune = true;
    };
    datasets."storix-main/LXD/custom" = syncPlan;
    datasets."storix-cold/BCK/custom" = syncPlan // {
      autosnap = false;
      autoprune = true;
    };
    datasets."storix-main/HOT/custom" = syncPlan;
    datasets."storix-cold/BCK/custom-hot" = syncPlan // {
      autosnap = false;
      autoprune = true;
    };
    datasets."storix-cold/LXD/custom" = syncPlan;
  };
  services.syncoid = {
    enable = true;
    commonArgs = [ "--no-sync-snap" ];
    commands."storix-main/LXD/containers" = {
      target = "storix-cold/BCK/containers";
      recursive = true;
    }; 
    commands."storix-main/LXD/custom" = {
      target = "storix-cold/BCK/custom";
      recursive = true;
    };
    commands."storix-main/HOT/custom" = {
      target = "storix-cold/BCK/custom-hot";
      recursive = true;
    };
  };
}
