{ config, pkgs, lib, ... }:
with lib; with strings;

let

  # Function definitions
  # ===================================================================================================================

  # Container interfaces
  # -------------------------------------------------------------------------------------------------------------------

  containerInterfaces = mapAttrs' (name: address: { name = "lxd-${name}"; value = {
    name = "lxd-${name}";
    addresses = [ { addressConfig = {
      Address = "${config.deployment.internalIPv4}/32"; Peer = "${address}/32";
    };} ];
    linkConfig = { RequiredForOnline = "no"; };
  }; }) config.deployment.containerMap;

  # Container hosts
  # -------------------------------------------------------------------------------------------------------------------

  serviceHost = name: address: "${address} ${name}.lebib.lan";
  serviceHosts = concatStringsSep "\n" (mapAttrsToList serviceHost config.deployment.containerMap);

in

{

  # Configuration values
  # ===================================================================================================================

  # Generic networking settings
  # -------------------------------------------------------------------------------------------------------------------

  networking = {
    hostName = "storix";
    hostId = "c54ba123";
    useDHCP = false;
    firewall.enable = false;
    wireguard.enable = false;
    nameservers = [ "127.0.0.1" ];
    search = [ "lebib.org" ];
    extraHosts = ''
      ${serviceHosts}
      10.14.255.255 roko.pstch.net roko
      10.14.255.224 notix.pstch.net notix
      10.13.255.208 bibix.lebib.lan bibix
      10.13.159.254 mail.lebib.lan
    '';
    iproute2.rttablesExtraConfig = ''
      13202 uplink
    '';
  };
  systemd.network.enable = true;

  # Virtual networks
  # -------------------------------------------------------------------------------------------------------------------


  # Interface definitions
  # -------------------------------------------------------------------------------------------------------------------

  # systemd.services.systemd-networkd.environment.SYSTEMD_LOG_LEVEL = "debug";

  systemd.network.networks = {
    # Loopback interface
    loopback = {
      name = "lo";
      address = [ "127.0.0.1/8" "10.13.255.222/32" ];
      extraConfig = ''
        [RoutingPolicyRule]
        FirewallMark=13202
        Table=13202
      '';
    };

    # Uplink bridge to `storix.lebib.org`
    enp0s3 = {
      name = "enp0s3";
      dns = [ "80.67.169.12" "80.67.169.40" ];
      addresses = [
        { addressConfig = {
          Address = "10.13.255.222/32"; Peer = "10.13.255.202/32"; }; }
      ];
      routes = [
        { routeConfig = {
          Destination = "0.0.0.0/0"; Gateway = "10.13.255.202"; }; }
      ];
    };
  } // containerInterfaces;

  # Traffic control
  # -------------------------------------------------------------------------------------------------------------------

  systemd.services."tc-tun-uplink" = {
    requires = [ "sys-subsystem-net-devices-tun\\x2duplink.device" ];
    after = [ "sys-subsystem-net-devices-tun\\x2duplink.device" ];
    wantedBy = [ "network-online.target" ];
    serviceConfig = { Type = "oneshot"; RemainAfterExit = true; };
    script = ''
      ${pkgs.iproute}/bin/tc qdisc del dev tun-uplink root 2>/dev/null || true
      ${pkgs.iproute}/bin/tc qdisc add dev tun-uplink root handle 1:0 htb default 1
      ${pkgs.iproute}/bin/tc class add dev tun-uplink parent 1:0 classid 1:1 htb rate 200mbit burst 20mbit
      ${pkgs.iproute}/bin/tc qdisc add dev tun-uplink parent 1:1 handle 2: fq_codel
    '';
  };

  systemd.services."init-netfilter-tables" = {
    wantedBy = [ "network-online.target" ];
    serviceConfig = { Type = "oneshot"; RemainAfterExit = true; };
    script = ''
      ${pkgs.iptables}/bin/iptables -L
      ${pkgs.iptables}/bin/ip6tables -L
    '';
  };

}

