{ config, pkgs, lib, ... }:
{
  imports = [
    (import (builtins.fetchGit {
      url = https://framagit.org/bib/dev/nix/check-mk-agent.git;
      rev = "e10cd10dfa2e3690047e3d48346940cf52ac73ae";
    }))
  ];
  services.check-mk-agent.enable = true;

  i18n.defaultLocale = "en_US.UTF-8";
  console.keyMap = "us";
  console.font = "Lat2-Terminus16";
  time.timeZone = "Europe/Paris";

  #environment.noXlibs = true;

  services.udisks2.enable = false;
  security.polkit.enable = false;
  security.audit.enable = true;

  # Tweak from https://github.com/lxc/lxd/blob/master/doc/production-setup.md
  security.pam.loginLimits = [
    {
      domain = "*";
      item = "nofile";
      type = "soft";
      value = "1048576";
    }
    {
      domain = "*";
      item = "nofile";
      type = "hard";
      value = "1048576";
    }
    {
      domain = "root";
      item = "nofile";
      type = "soft";
      value = "1048576";
    }
    {
      domain = "root";
      item = "nofile";
      type = "hard";
      value = "1048576";
    }
  ];



  environment.systemPackages = with pkgs; [ 
    # Monitoring tools
    htop atop
    # Text Editor
    vim
    # Git 
    git
    # NixOS image generator
    nixos-generators
  ];

  services.getty.autologinUser = "root";
  services.qemuGuest.enable = true;
  programs.ssh.setXAuthLocation = true;
  services.openssh.enable = true;
#  services.openssh.forwardX11 = true;
#  services.check-mk-agent.enable = true;
}
