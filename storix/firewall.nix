# BIB service router firewall definition
# =================================================================================================
# Tasks :
#  - TODO: make global definitions use values from deployment.*, instead of
#          defining them here
#  - TODO: switch to nftables
#  - TODO: setup an HTTP proxy server and isolate backend containers
#          from external networks
#  - XXX: maybe add some rate-limited logging
#
# This firewall is used to :
#  - restrict the set of services externally exposed by this host, in order to
#    limit the damage in case of a misconfiguration on the host itself
#  - isolate the containers from the rest of the network and from each other to
#  - limit damage in case of penetration of one of the containers
#  - allow unrestricted management traffic
#
# This firewall is NOT used to :
#  - protect the expose services against DoS attacks, as this should be done on
#    the edge of the network
#
# These firewall rules handle :
#  - Filtering incoming connection :
#     + ACCEPT ICMP, tunnels and manageement traffic
#     + ACCEPT protocols for exposed services
#     + ACCEPT established connections
#     + REJECT for traffic from internal network, DROP remaining traffic
#  - Filtering forwarded connections :
#     + ACCEPT traffic from containers to the external network
#     + ACCEPT management traffic
#     + ACCEPT established connections
#     + REJECT for traffic from internal network, DROP remaining traffic
#  - Translating forwarded connections :
#     + MASQUERADE traffic from containers to the external network
#  - Mangling forwarded connections :
#     + CLAMP MSS to PMTU for traffic going through the uplink tunnel
#
# INPUT RULES are splitted in three chains :
#  - INPUT_INTERNAL : traffic from internal uplink (through notix.pstch.net)
#  - INPUT_EXTERNAL : traffic from external network (through roko.pstch.net)
#  - INPUT_CONTAINERS : traffic from LXD containers

{ config, pkgs, lib, ... }:

{
  boot.kernel.sysctl."net.ipv4.ip_forward" = true;
  environment.etc.ethertypes.source = "${pkgs.iptables}/etc/ethertypes";
  services.ferm = {
    enable = true;
    config = ''

      # Global definitions
      # -------------------------------------------------------------------------------------------

      @def $EXT_IF = tun-uplink;                                       # External iface
      @def $INT_IF = enp0s3;                                           # Uplink iface
      @def $CNT_IFS = lxd-+;                                           # Container ifaces
      @def $MGMT_TUN_PORT = 53248;                                     # Management tunnel port
      @def $UPLINK_TUN_PORT = 53208;                                   # Uplink tunnel port
      @def $TUN_PORTS = ($MGMT_TUN_PORT $UPLINK_TUN_PORT);             # Internal tunnel ports
      @def $MGMT_NETS = (10.14.240.240 10.14.248.0/24                  # pstch.net management nets
                         10.13.240.240 10.13.248.0/24                  # lebib.org management nets
			 10.17.24.3/32);			       # infasg.org mnagement nets
      @def $SVCS_NET = 10.13.208.0/23;                                 # Application containers net
      @def $MEDI_NET = 10.13.221.0/23;                                 # Application containers net
      @def $STORI_NET = 10.13.222.0/23;                                 # Application containers net
      @def $USER_NET = 10.13.210.0/23;                                 # User containers net
      @def $CORE_NET = 10.13.240.0/24;                                 # Core containers net
      @def $CNT_NETS = ($SVCS_NET $MEDI_NET $STORI_NET $USER_NET $CORE_NET);                # Containers nets
      @def $INTERNAL = 10.0.0.0/8;                                     # Internal net

      # Incoming/outgoing traffic
      # -------------------------------------------------------------------------------------------

      table filter {
        chain INPUT {                                                  ### Incoming traffic
          interface lo ACCEPT;                                         # Allow loopback traffic
          proto icmp ACCEPT;                                           # Allow ICMP traffic
	  saddr 10.17.24.3/32 proto tcp dport 6556 ACCEPT; 
          mod state state (RELATED ESTABLISHED) ACCEPT;                # Allow established conns
          mod state state (INVALID) DROP;                              # Drop invalid conns
          interface $INT_IF jump INPUT_INTERNAL;                       # Jump to INPUT_INTERNAL
          interface $EXT_IF jump INPUT_EXTERNAL;                       # Jump to INPUT_EXTERNAL
          interface $CNT_IFS jump INPUT_CONTAINERS;                    # Jump to INPUT_CONTAINERS
          saddr $MGMT_NETS ACCEPT;                                     # Allow management traffic
          saddr $INTERNAL REJECT reject-with icmp-port-unreachable;    # Reject from internal
          policy DROP;                                                 # Drop remaining traffic
        }
        chain INPUT_INTERNAL {                                         ### Internal traffic
          proto udp dport ($TUN_PORTS) ACCEPT;                         # Allow tunnel traffic
          jump INPUT_EXTERNAL;                                         # Jump to INPUT_EXTERNAL
        }
        chain INPUT_EXTERNAL {                                         ### External traffic
          proto tcp dport 22 ACCEPT;                                   # Allow SSH traffic
        }
        chain INPUT_CONTAINERS {                                       ### Container traffic
          proto tcp dport (80 443) ACCEPT;                             # Allow HTTP(S) traffic
        }
        chain OUTPUT {                                                 ### Outgoing traffic
          policy ACCEPT;                                               # Allow all
        }
      }

      # Forwarded traffic
      # -------------------------------------------------------------------------------------------

      table filter {
        chain FORWARD {                                                ### Forwarded traffic
          mod state state (RELATED ESTABLISHED) ACCEPT;                # Allowed established conns
          mod state state (INVALID) DROP;                              # Drop invalid connections
          saddr $MGMT_NETS ACCEPT;                                     # Allow management traffic
          interface $EXT_IF jump FORWARD_EXTERNAL;                     # Jump to FORWARD_CONTAINERS
          interface $CNT_IFS jump FORWARD_CONTAINERS;                  # Jump to FORWARD_CONTAINERS
          saddr $INTERNAL REJECT reject-with icmp-host-unreachable;    # Reject traffic internal
          policy DROP;                                                 # Drop remaining traffic
        }
        chain FORWARD_EXTERNAL {                                       ### External traffic
        }
        chain FORWARD_CONTAINERS saddr $CNT_NETS {                     ### Container traffic
          proto icmp ACCEPT;
          daddr ! $INTERNAL ACCEPT;                                    # Accept to external nets
          daddr 10.13.159.254 proto tcp dport 25 ACCEPT;               # SMTP server
          daddr 10.13.240.36 proto tcp dport (3306 5432) ACCEPT;       # SQL servers
        }
      }

      # Translation/mangling
      # -------------------------------------------------------------------------------------------

      table nat {
        chain PREROUTING interface $EXT_IF {                           ### Incoming from external
        }
        chain POSTROUTING outerface $EXT_IF {                          ### Outgoing to external
          daddr ! $INTERNAL MASQUERADE;                                # Masquerade to iface addr
        }
      }
      table mangle chain FORWARD outerface $EXT_IF {                   ### Outgoing to external
        proto tcp tcp-flags (SYN RST) SYN TCPMSS clamp-mss-to-pmtu;    # Reduce MSS to tunnel's MTU
      }
    '';
  };
}

