{ config, lib, pkgs, ... }:

{
  imports = [
    (import (builtins.fetchGit {
      url = https://framagit.org/bib/dev/nix/irq-affinity.git;
      rev = "1d635141e727bc518ff1ceee45debed4883d492e";
    }))
  ];

  boot.loader.grub.enable = true;
  # NOTE: remember to add other devices in the mirror, and sync their /boot
  boot.loader.timeout = 1;
  boot.loader.grub.devices = [ "/dev/disk/by-id/wwn-0x5002538e406e17da" ];

  #boot.kernelPackages = pkgs.linuxPackages_hardened;
  #boot.kernelPatches = lib.singleton {
  #  name = "enable-32bit-emulation";
  #  patch = null;
  #  extraConfig = ''
  #    IA32_EMULATION y
  #  '';
  #};

  # configure Linux kernel command line
  boot.kernelParams = [
    # enable passthrough for integrated graphics and audio
    "vfio-pci.ids=8086:5912,8086:a121,8086:a170,8086:a123"

    # disable framebuffers to avoid initializing integrated graphics
    "video=efifb:off,vesafb:off" "nomodeset"

    # enable the IOMMU
    "intel_iommu=on" "iommu=on"

    # limit the ZFS ARC cache size
    "zfs.zfs_arc_max=1073741824"

    # secondary console on TTY, primary console on serial (COM1)
    # "console=tty0" "console=ttyS0"

    # networking config (@despo)
    "ip=192.168.1.224::192.168.1.254:255.255.255.0::eth1:off"
    "ip=192.168.1.224::192.168.1.254:255.255.255.0::eno1:off"

    # networking config (management)
    "ip=192.168.2.224::192.168.2.254:255.255.255.0::eth2:off"
    "ip=192.168.2.224::192.168.2.254:255.255.255.0::eno0:off"

    # page poisoning
    # "page_poison=on" "slub_debug=P"
  ];

  boot.extraModprobeConfig = ''
    options kvm-intel nested=Y
  '';
  boot.kernelModules = [
    "pcspkr"                              # beeper (XXX: does not work)
    "nct6775"                             # thermal sensors
    "vfio-pci"                            # VFIO passthrough
    "nvme"                                # NVMe storage
  ];

  boot.blacklistedKernelModules = [
    "i915"                                # integrated graphics
    "snd_hda_intel" "snd_hda_codec_hdmi"  # integrated audio
    "i2c_i801" "i2c_smbus"                # associated devices (by the IOMMU)
  ];

  boot.kernel.sysctl = {
    "vm.overcommit_memory" = "1";
    "net.ipv4.ip_forward" = "1";
  };

  boot.irqAffinity = {
    enable = true;
    default = "80";
    override = "80";
    overrides = {"149" = "c0";};
  };

  boot.zfs.requestEncryptionCredentials = true;

  boot.initrd.availableKernelModules = [ "e1000e" "igb" ];
  boot.initrd.network.enable = true;
  boot.initrd.network.ssh.enable = true;
  boot.initrd.network.ssh.port = 23;
  boot.initrd.network.ssh.hostKeys = [
    "/etc/keys/ssh-initrd/ssh_host_rsa_key"
    "/etc/keys/ssh-initrd/ssh_host_ed25519_key"
  ];
  boot.initrd.network.postCommands = ''
    echo "zfs load-key notix-main; killall zfs" >> /root/.profile
  '';
  boot.initrd.postMountCommands = ''
    cp -rf /mnt-root/etc/keys /etc/keys
    echo | zfs load-key -a || true
  '';
}
