{ config, lib, pkgs, ... }:

{
  environment.etc.ethertypes.source = "${pkgs.iptables}/etc/ethertypes";
  services.ferm = {
    enable = true;
    config = ''
      @def $EXT_IF = tun-roko;                                         # External uplink iface
      @def $INT_IF = (br-uplink-1 br-uplink-2);                        # Internal uplink iface
      @def $INT_ADDR = 192.168.1.224;                                      # Internal uplink address
      @def $VM_IFS = (br-bibix br-tinix);                              # Backend ifaces
      @def $MGMT_TUN_PORT = 55224;                                     # Management tunnel port
      @def $UPLINK_TUN_PORT = 54224;                                   # Uplink tunnel port
      @def $TUN_PORTS = ($MGMT_TUN_PORT $UPLINK_TUN_PORT);             # Internal tunnel ports
      @def $MGMT_NETS = (10.14.240.240 10.14.248.0/24);                # Management nets
      @def $VM_NETS = (10.13.255.208 10.14.255.208);                   # VM nets
      @def $INTERNAL = 10.0.0.0/8;                                     # Internal nets

      table filter {
        chain INPUT {                                                  ### Incoming traffic
          interface lo ACCEPT;                                         # Allow loopback traffic
          proto icmp ACCEPT;                                           # Allow ICMP traffic
          mod state state (RELATED ESTABLISHED) ACCEPT;                # Allow established conns
          mod state state (INVALID) DROP;                              # Drop invalid conns
          interface $INT_IF jump INPUT_INTERNAL;                       # Jump to INPUT_INTERNAL
          interface $EXT_IF jump INPUT_EXTERNAL;                       # Jump to INPUT_EXTERNAL
          interface $VM_IFS jump INPUT_VMS;                            # Jump to INPUT_VMS
          saddr $MGMT_NETS ACCEPT;                                     # Allow management traffic
          saddr $INTERNAL REJECT reject-with icmp-port-unreachable;    # Reject from internal
          policy DROP;                                                 # Drop remaining traffic
        }
        chain INPUT_INTERNAL {                                         ### Internal traffic
          proto udp dport ($TUN_PORTS) ACCEPT;                         # Allow tunnel traffic
          proto tcp dport 22 ACCEPT;                                   # Allow SSH traffic
          jump INPUT_EXTERNAL;                                         # Jump to INPUT_EXTERNAL
        }
        chain INPUT_EXTERNAL {                                         ### External traffic
          proto udp dport 54224 ACCEPT;
        }
        chain INPUT_VMS {                                              ### VM traffic
        }
        chain OUTPUT {                                                 ### Outgoing traffic
          policy ACCEPT;                                               # Allow all
        }
        chain FORWARD {                                                ### Forwarded traffic
          mod state state (RELATED ESTABLISHED) ACCEPT;                # Allowed established connections
          mod state state (INVALID) DROP;                              # Drop invalid connections
          interface $INT_IF jump FORWARD_INTERNAL;                     # Jump to FORWARD_INTERNAL
          interface $VM_IFS jump FORWARD_VMS;                          # Jump to FORWARD_VMS
          saddr $MGMT_NETS ACCEPT;                                     # Allow management traffic
          saddr $INTERNAL REJECT reject-with icmp-host-unreachable;    # Reject traffic from internal network
          policy DROP;                                                 # Drop remaining traffic
        }
        chain FORWARD_INTERNAL {                                       # Forwarded internal traffic
          daddr 10.13.255.208 proto udp dport (53208 53248) ACCEPT;    # Allow Wireguard traffic to BIB VM
          daddr 10.14.255.208 proto udp dport (54208 54248) ACCEPT;    # Allow Wireguard traffic to PST VM
        }
        chain FORWARD_VMS {                                            # Forwarded VM traffic
          saddr $VM_NETS daddr ! $INTERNAL ACCEPT;                     # Allow VM traffic to external networks
        }
      }
      table nat chain PREROUTING interface $INT_IF daddr $INT_ADDR {   ### Incoming local translated traffic
        proto udp dport 53208 DNAT to-destination 10.13.255.208:53208; # Translate BIB uplink VPN
        proto udp dport 53248 DNAT to-destination 10.13.255.208:53248; # Translate BIB mgmt VPN
        proto udp dport 54208 DNAT to-destination 10.14.255.208:54208; # Translate BIB uplink VPN
        proto udp dport 54248 DNAT to-destination 10.14.255.208:54248; # Translate BIB mgmt VPN

      }
      table nat chain POSTROUTING outerface $INT_IF {                  ### Outgoing forwarded traffic
          saddr $VM_NETS daddr ! $INTERNAL MASQUERADE;                 # Masquerade containers to external
      }
      table mangle chain FORWARD outerface $EXT_IF {                   ### Mangled traffic
        proto tcp tcp-flags (SYN RST) SYN TCPMSS clamp-mss-to-pmtu;    # Reduce MSS to tunnel's MTU
      }
    '';
  };
}
