{ config, lib, pkgs, ... }:

{
  imports = [
    (import (builtins.fetchGit {
      url = https://framagit.org/bib/dev/nix/libvirt-domains.git;
      rev = "3bfc1361bd257cef54fc63bf8b8d703dcf6f3522";
    }))
  ];

  environment.shellAliases.virsh = "virsh --connect=qemu:///system";
  systemd.services.libvirtd.serviceConfig.LimitMEMLOCK = "infinity";

  virtualisation = {
    libvirtd = {
      enable = true;
      # Allow UEFI VMs
      qemuOvmf = true;
      # On shutdown, use managedsave to "suspend" the VMs
      onShutdown = "suspend";
      # QEMU settings (enable SPICE and tweak the sandboxing)
      qemuVerbatimConfig = ''
        # spice_listen = "0.0.0.0"
        # spice_tls = 1
        # spice_tls_x509_cert_dir = "/etc/pki/libvirt-spice"
        cgroup_device_acl = [
          "/dev/null", "/dev/full", "/dev/zero",
          "/dev/random", "/dev/urandom", "/dev/ptmx",
          "/dev/kvm", "/dev/kqemu",
          "/dev/rtc", "/dev/hpet", "/dev/net/tun",
          "/dev/vfio/vfio", "/dev/vfio/2", "/dev/vfio/11"
        ]
        seccomp_sandbox = 0
      '';

      domains = let
        mkInterface = macAddress: bridge: {
          inherit macAddress; source = { inherit bridge; }; address.slot = "0x3";
        };
      in {

        # home-ws-nix = {
        #   # Machine metadata
        #   hostname = "statix";
        #   domain = "pstch.net";
        #   type = "kvm";
        #   description = ''
        #     Address  : 192.168.1.128
        #     Guest OS : NixOS 19.03
        #     CPU      : 4 core, no throttle
        #     Memory   : 4G (max 4G)
        #     Swap     : 16G NVMe SSD
        #     Storage  : 40G SSD, 256G HDD
        #   '';
        #   # VFIO passthrough not compatible with Q35 and managed save
        #   libvirt.shutdownMethod = "shutdown";
        #   os.machine.chipset = "i440fx";
        #   # CPU configuration
        #   vcpu = 4; cpuset = "4,5,6,7";
        #   # Memory configuration
        #   memory = 4096;
        #   # Network configuration
        #   interfaces = [ (mkInterface "52:54:00:36:8e:8b" "default") ];
        #   # Storage configuration
        #   disks = mkDisks "home-ws-nix" "pstch.net";
        #   # QEMU configuration
        #   qemu.seabiosDebugLog = "/tmp/seabios_debug-home-ws-nix.log";
        #   # Passed through devices
        #   extraDevices = lib.strings.concatStrings [
        #     # ''<!-- USB input devices -->
        #     # <hostdev mode='subsystem' type='usb' managed='yes'>
        #     #   <!-- Razer USA, Ltd Abyssus 2014 -->
        #     #   <source>
        #     #     <vendor id='0x1532'/>
        #     #     <product id='0x0042'/>
        #     #   </source>
        #     #   <alias name='hostdev0'/>
        #     #   <address type='usb' bus='0' port='1'/>
        #     # </hostdev>
        #     # <hostdev mode='subsystem' type='usb' managed='yes'>
        #     #   <!-- Logitech, Inc. Keyboard K120 -->
        #     #   <source>
        #     #     <vendor id='0x046d'/>
        #     #     <product id='0xc31c'/>
        #     #   </source>
        #     #   <alias name='hostdev1'/>
        #     #   <address type='usb' bus='0' port='2'/>
        #     # </hostdev>
        #     # ''
        #     ''<!-- VFIO passthrough -->
        #     <hostdev mode='subsystem' type='pci' managed='yes'>
        #       <!-- Intel HD Graphics 630 (rev 04) -->
        #       <driver name='vfio'/>
        #       <source>
        #         <address domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
        #       </source>
        #       <alias name='hostdev2'/>
        #       <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
        #     </hostdev>''
        #   ];
        # };

        bibix = {
          # Machine metadata
          hostname = "bibix";
          type = "kvm";
          domain = "lebib.org";
          description = ''
            Type     : KVM (IOMMU/VT)
            Address  : 10.14.224.9
            Guest OS : Debian GNU/Linux 10.0 (Buster)
            CPU      : 2 cores
            Memory   : 4G (max 4G)
            Storage  : main:40G/SSD hot:40G/SSD cold:1T/HDD
          '';
          # CPU configuration
          vcpu = 6;
          cpuset = "0,1,2,3,4,5";
          # Memory configuration
          memory = 24576;
          # Network configuration
          interfaces = [ (mkInterface "52:54:00:a4:ef:ea" "br-bibix") ];
          # Storage configuration
          disks = [ ];
          extraDevices = ''
      <disk type='block' device='disk'>
	      <driver name='qemu' type='raw'/>
	      <source dev='/dev/disk/by-id/ata-ST31000340NS_9QJ4TFEW'/>
	      <target dev='sdd' bus='scsi'/>
	      <address type='drive' controller='0' bus='0' target='0' unit='3'/>
	    </disk>
	    <disk type='block' device='disk'>
	      <driver name='qemu' type='raw'/>
	      <source dev='/dev/disk/by-id/ata-ST31000340NS_9QJ4ZELG'/>
	      <target dev='sde' bus='scsi'/>
	      <address type='drive' controller='0' bus='0' target='0' unit='4'/>
	    </disk>
	    <disk type='block' device='disk'>
	      <driver name='qemu' type='raw'/>
	      <source dev='/dev/disk/by-id/ata-ST31000340NS_9QJ4V2F2'/>
	      <target dev='sdf' bus='scsi'/>
	      <address type='drive' controller='0' bus='0' target='0' unit='5'/>
	    </disk>
	    <disk type='block' device='disk'>
	      <driver name='qemu' type='raw'/>
	      <source dev='/dev/disk/by-id/ata-ST31000340NS_9QJ5A6MW'/>
	      <target dev='sdg' bus='scsi'/>
	      <address type='drive' controller='0' bus='0' target='0' unit='6'/>
	    </disk>
	    <disk type='block' device='disk'>
	      <driver name='qemu' type='raw'/>
	      <source dev='/dev/disk/by-id/nvme-Samsung_SSD_970_EVO_Plus_250GB_S4EUNG0M228357A'/>
	      <target dev='sdc' bus='scsi'/>
	      <address type='drive' controller='0' bus='0' target='0' unit='2'/>
	    </disk>
          '';
        };

        # ian-vm-deb = {
        #   # Machine metadata
        #   hostname = "ianix";
        #   domain = "pstch.net";
        #   type = "kvm";
        #   description = ''
        #     Address  : 10.14.224.17
        #     Guest OS : Debian GNU/Linux 10.0 (Buster)
        #     CPU      : 1 core, factor 0.5
        #     Memory   : 1G (max 2G)
        #     Swap     : 4G NVMe SWAP
        #     Storage  : 20G SSD, 64G HDD
        #   '';
        #   # CPU configuration
        #   vcpu = 1;
        #   cpuset = "2,3";
        #   # Memory configuration
        #   memory = 1024;
        #   # Network configuration
        #  interfaces = [ (mkInterface "52:54:00:81:84:dd" "hosted-vms") ];
        #   # Storage configuration
        #   disks = mkDisks "ian-vm-deb" "pstch.net";
        #   # QEMU configuration
        #   qemu.seabiosDebugLog = "/tmp/seabios_debug-ian-vm-deb.log";
        #   # Extra configuration
        #   extraConfig = ''
        #     <!-- SPICE virtual GPU -->
        #     <graphics type='spice' listen='0.0.0.0' port='5900' tlsPort='5901'
        #               autoport='no' keymap='en-us' passwd='dontpanic'>
        #       <listen type='address' address='0.0.0.0'/>
        #     </graphics>
        #     <video>
        #       <model type='qxl' heads='1' primary='yes'
        #              ram='65536' vram='65536' vgamem='16384' />
        #       <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x0'/>
        #     </video>

        #     <!-- SPICE channels -->
        #     <channel type='spicevmc'>
        #       <target type='virtio' name='com.redhat.spice.0'/>
        #       <address type='virtio-serial' controller='0' bus='0' port='2'/>
        #     </channel>
        #     <channel type='spiceport'>
        #       <source channel='org.spice-space.webdav.0'/>
        #       <target type='virtio' name='org.spice-space.webdav.0'/>
        #       <address type='virtio-serial' controller='0' bus='0' port='3'/>
        #     </channel>

        #     <!-- SPICE USB redirections -->
        #     <redirdev bus='usb' type='spicevmc'>
        #       <address type='usb' bus='0' port='2'/>
        #     </redirdev>
        #     <redirdev bus='usb' type='spicevmc'>
        #       <address type='usb' bus='0' port='3'/>
        #     </redirdev>
        #   '';
        # };

        # alb-vm-deb = {
        #   # Machine metadata
        #   hostname = "apathie";
        #   domain = "pstch.net";
        #   type = "kvm";
        #   description = ''
        #     Address  : 10.14.224.25
        #     Guest OS : Debian GNU/Linux 10.0 (Buster)
        #     CPU      : 1 core, factor 0.5
        #     Memory   : 1G (max 2G)
        #     Swap     : 4G NVMe SSD
        #     Storage  : 20G SSD, 64G HDD
        #   '';
        #   # CPU configuration
        #   vcpu = 2; cpuset = "2,3";
        #   cputune.vcpupins = { "0" = 2; "1" = 3; };
        #   # Memory configuration
        #   memory = 1024;
        #   # Network configuration
        #   interfaces = [ (mkInterface "52:54:00:1d:6d:d7" "hosted-vms") ];
        #   # Storage configuration
        #   disks = mkDisks "alb-vm-deb" "pstch.net";
        #   # QEMU configuration
        #   qemu.seabiosDebugLog = "/tmp/seabios_debug-alb-vm-deb.log";
        # };
      };
    };
  };
}
