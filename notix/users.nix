{ config, lib, pkgs, ... }:

let
  ssh_keys = {
    monitor = [ 
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCsrG0LePph0oUCMOoIPHBkGjamdDjxFWuF1BooORwKcBmJu7b0/5AhSCt6CCQ5kHz+H5yLHCty2o2d1B/Dyu2NNe3o1XQAhYz3Gc74y7hVu9RTkoZ7xBN480JwyHfNzzktSM6qpFtEln+uVhxWxvJfjfZdkN7OEp9S3zTP77lkG87e6WjkjS9E+tkwtPeOxBEIhAtp6/MsUNRSEOF3kCyNIAnpLWg5XiVxSwENDjCKF/DupnZkdMmn2KesGAG7gJw4lTAFUHu2A4XkG2484TukB2YI23zQqK7qq90lIC/KeCF5fvtFOSC69T1VycVqmOT1XGgL0j02gj9sbfmXbkkIuslCmSHm/QqbKTiUsvJQkSaj3cpiXwdaIgKMlN1v1V/6tjrUz3MRlBb2qFDHZ0YU637pTWpE5Bk614DJUhOM0UXpSwdb8hdbdDiEK22bTM3sZbVw31Bif1BKFj26ZdUVAkQ4bSwuJJ6Wgwkimz400OzLxO6hyGc6T0Yj464VPsjM0qqRSGJOH7VuPNZONEZ/eOtw+J7OYf+VA97uHqQiWyd/XTws9z2wdy1ZLTQAKWH9CRTSVXSJeEg8wcdBvmxBbDROLBz+YretO3cx9civ7qkPxS4nbZ+2Ko81Q8YNT7MaU3DN4b2L26vo6PgVlrEgIEgSq8ai+CspNWtSaq1A4Q== benbart@ploper"
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDWE4gMOa792s86OkGf9f8qbFUKiP8yLjClyK5W7b1PXgDoIBJjjYvqrd7ADBUspwcrzWKdtiUyIq6NMYYxGp01JTy9lGJtVoDkTtkKdGaOovD2a4TlbQspZ7EozRSTJyIVw3b45aMsHVVBEy7ZVxljB/Ib9Yxzywd35rs6P+bOYTTC1QzhKDfoYwHT7VSbQMitPcUaikw5ALYcnp7SS1SqPTaivm6mqpeYbL+7lVxoZLFCpRHpmL37d7UJL77dic78Wwno7K0RhPp0qIsyQAFgEfCiDek8JKF/mG5B88sP5mlLS1TZN+dMW8nIKQbZ35A/YcswFB0JJtXpLPoWsfebGBsPL5QhbWWg6zbpmQOjlmXN4u8nYuhlY2cHBb0mT4gEey3EZsW6xNbu1umBKxChl3EsGCJX6u6AQPw7vAZtIkm30Nqh/CkvCaShabbBg63xUmwTleCVWICGrSwLaRbKQomfDkih/eIKI60QOEEbc+cmaZabG+DVuY3oQZH5wuvR6jzBeXk28JIDSJlBEHCNUccQw+pTvqLmDCVg0aIvvPTD32VwpQ67LFUEJ6Ucv/+LWBPhvo8b7nEIASTTTet14BBb7gjgcmbBUaoY76/5+8zjgUAy63h1YMVRU2XeoSE7EyXan0mf0Q/oQgBj6M4CuLiWfWT0Mhvgsf3jrt3pAw== monitor@checkmk.bakou.pro" ];
    benbart = [
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiLtjyaq5VAFIQwEy+HJa5Hf4a8/zHuwp3nuW5LXCiQwAAt+/byOkgqFUjF7I0bsYIIaoMEbCCjkIKfpvX+/wZqBJvJ4fpvqaK48yl8Qpir+VPmgruEInF5Ovm696q2Fj/fHTEzr9NO5uKUfgdnyvYr0L9HVZMp+PmgHnyvBMo5LvpQ/HS8ayXTL8dyk+GfYqjtdzR0kTF0YXZ4udRSlXCazUEDc9GbQD6aoOdiWnnDQ++j5AN/4LTd1gluC9ywVo3/AWBSxQe/d+UQm8GOaM4QXLNmO5C5XQS4jKznir/s7MHkd0WzaWxFux5+7IkXqtkx/omPlZEs/cKLob648If cardno:000500005F42" 
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC7RidQzA/IF7mkvquIgivY4qIHL0v3piJagB01NkaVJF6e3fX1QBOPt4Nzx3lihRs4WVlBaPNZ/mJaNvPqdG+1VdWd/7Hgktg7D/7cILquUlzWCp7v2eJt1uvkLsr3JjB/bJ9rIb96QpsGP+ftddwpRgybLVVGcVU3tTF8t+ZrOkZ3UhSvc5xa/NppbFaHDZbHqAbiirLxJKr54RKkuiKpqb/YCJM+lR0qf6sxtqOL2ZsaO8D9G2GxsmBq0tkjakmJ6oO0C7xzF2StR6DpPykKT4gBy64duntkHbBzP0cIE8nrJcB4B9baHv+2EmcC0LSu0QDJ8FfJFba3O2GdCefpYsoGJ3XszfyFG9IIEddtlMrxuEYBdDMLkoKFmqRW7uftWJgJAtnimQPFyvS4oov4cDlfbDIXjJNHoUP+mEz36UBP7J3IETbhgXRv03v86FhHoHPZVXFOPhsZDVkQR+JLWlpOUEaFLCv+4D7oxKqWbLOL6At5ELE9lsIIY9kwZBBIF+Ju/JWv6y5eKlWUukbzHOQJQ/iTL4TK0WCrPt2oUiDwIXMfIbogRASJ1KzCQcncSJXuNx9jvaq08COVhVixHrVsu3T71sCvqkYBc4ePHeZfeea5Rl5WRJhNL7Tt6J6fqvmAynYWmJtaz/PdeBn0DbhamFrgROgLksyOg3QfJQ== bchatelain@pc46" ];
    pistache = [ 
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDAP7dxz43XD7OVFHLRIvp7QLNk3QD8VCuWkOYsrfFLp3gX9mD5/YIGp0pfAUG2eZjwcA1ef0fCERChwUqO4Wrz0xaQ1PD7hlC+6XqDCwCDNEGnufOZbilQxlpuroZUQ/TEVhbHYxTK2hyV63eVvDh56dI3IqeoxqU1oY5BnXZjHJYkWPbkIxhsgGTlW5nJfkgZyKDie1O79NJdzQmqVuZt5z6yKhnuX2+gOgRC5hfto8tWLZj9Hy9d15VKL1b0VfuCK+mpANRhQds7YsfMfTTYMIDfuFCI8ZpYWQxNw7t8QXbfLaMsiRgphGZEr555g+xdkXCZ2+Oj4rlPfKgMzXLQ2gFv0Aen4wwzEdjhXdiwuKbbbsI4WKd3kvX5Nc70wKYxvI8fQs0GlO9FLdEbC8R6J15JKREmJlgizImmqb8KIME4bGsYGOVPFVbRHQKjUT1rFVWD3+aDml80cAlHOpfoln7C4oqCi+oDDmAuyH358O2S4EdIGSGe4I2i0eJvxn3EA8ZVLKUWj/lcyJ05eFHdLZWcLyC4op8faNGqhdgG0I/tRJ/sFgpCj0Z7b5afMKxdX/LHYtOqcpxoQpT/mwIEQe6VPpUHQC7xrOB/kKIc5DNSVZdIQnaqRYvUrgPrWhXR9MG3/7Gt4lq0j39n1+Fm7aj6L9CmLHc3vfwCttv/9Q== openpgp:0x451F2F0F" ];
    smyds = [
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCoerbEBysWo+cJ6SfWTJuCadhJcwGvPdsGF4dmzJrK/X5v8impivie1s7iS8dfYUsUgiAolVqaKAa1cE+E5J5VHRv+bMbpbUjeLR3YCtBtCnr4Zp9YOwsgPH7QN6O7kuZTv9RaoXbDs/OVM1UnYIzFVwoBiiEokqBgCwy0RYo400raFJ+xRk4x/qyUsYLn1jxo3h/z3IK1V2BuaXEa2g7n1/CG6wSgIZ5z6TiCWp0sRFLWpfmqAoWML71SdLl+/opy57zrPO0bUtCKFzupTo3MzyQdaFavm5OXuId5y1qm4zi/X6fHTljwU3igkvmvsjjCvxTbamgouxyhEl5b2HnX smyds@riseup.net"];
    wargreen = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBRUVHxWsppYlotxeFzTA8HHROSDW/P6nKjAeD9aqUR+QVhiK88mBmTAYjjPbWrxXR214O3u45I5RykyNks5llfYndGcb0tuapZU8T/ELQBHNfAeVl6FxBx4iDYfZ3B3DpSkDk5Rg8HO09qfUAI5S5SUcSIkDQxEufASqBA2cY4KQMgMISz0K4Dy3JelibGBXCCzW+e2ggECyoUfXseQc9Q7CkRH8r2F2xYPQQRA4LWsfcENNzqL/9n737l+QWWLeaJdBx4L+zfYXxNxnciSHctHxXSBUp6WE6yTsA6UzrwfrlDJvmALEQtOa7hlFcc/l9055kmAjRswEe3XUh8rYv wargreen@LaChoze"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXDiGOe86c6RdKktNScQ3VVfhl/dPqJMd9FNtrhgZDLJ3A+0P4E97HVWrahWtYMumsYv133ZdOT1rBNJ61XwqVBNAa22DRg3k0w0ADsMVkOZYr7RbOqbVPT1uL/AFMbo6pDYTYCkVC+/0G2zclOHELaYJg0ZwO1wyMhfmtDWTwxfADiSsb1oq6ve+ybrwc/FMq0iyDRw5zP/8lJGSEGDJTmwbzo54NlMiAVrW+Kp5iVcGk7FGjak3wAiShZTfTiGATDNW/ET1gHTl2m8/QxM04bb5GzTPcXKU+1No4SW3ptAhHPAAB+Y9NgGkZXLpr4euYZDQnz2x+DsTPgxOYbUCZ4OQSBtBVVTAaVj88fLh9/oqmBpD3SSbhjS52q+6sDozDzfMwwjPZ3fHul3ispKbrZVTS6oRbRCyhLCAe6yGH9E+pi1xIaiYzhZQxXGGJos4w9F8BdNxWSAPLsC/3GlSQTcMjAGKa/6fntJ3ZKRXPFU4mcxZEka2kWOlosCDFcTM= wargreen@fetide"
    ];
  };
in

{
  security.sudo.wheelNeedsPassword = false;

  users.motd = ''
    +---------------+--------------------------------------------------------------+
    |  NixOS 19.09  |  notix.pstch.net, bare-metal host                            |
    |   \\  \\ //   |    Admins      : pistache                                    |
    |  ==\\__\\/ // |    Services    : KVM host, WireGuard, BIRD, haproxy          |
    |    //   \\//  |    Router-ID   : 10.14.255.224                               |
    | ==//     //== |    CPU         : Intel i7-7700K (4) @ 4.200GHz               |
    |  //\\___//    |    Memory      : 15848 MiB                                   |
    | // /\\  \\==  |    Storage     : SSD NVMe (2*250GB), SSD SATA (2*250GB)      |
    |   // \\  \\   |                  HDD SATA (2*1TB, 1*250GB)                   |
    +---------------+--------------------------------------------------------------+
  '';

  users.users.pistache = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.pistache;
  };

  users.users.smyds = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.smyds;
  };

  users.users.benbart = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.benbart;
  };

  users.users.wargreen = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.wargreen;
  };
  users.users.monitor = {
    isNormalUser = true; extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = ssh_keys.monitor;
  };


  users.users.root.openssh.authorizedKeys.keys = lib.concatLists (lib.attrValues ssh_keys);
  boot.initrd.network.ssh.authorizedKeys = lib.concatLists (lib.attrValues ssh_keys);

}
