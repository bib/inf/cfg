{ config, pkgs, lib, ... }:
{

  networking = {
    hostName = "notix";
    hostId = "17acab47";
    useDHCP = false;
    wireguard.enable = true;
    firewall.enable = false;
    nameservers = [ "127.0.0.1" ];
    iproute2.enable = true;
    iproute2.rttablesExtraConfig = ''
      22400 internal
    '';
  };

  systemd.network.enable = true;
  systemd.network.netdevs = {
    # Uplink bridge (@despo)
    br-uplink-1 = {
      netdevConfig = { Kind = "bridge"; Name = "br-uplink-1"; };
      extraConfig = ''
        [Bridge]
        STP=off
      '';
    };
    # Uplink bridge (@benbart)
    br-uplink-2 = {
      netdevConfig = { Kind = "bridge"; Name = "br-uplink-2"; };
      extraConfig = ''
        [Bridge]
        STP=off
      '';
    };
    # Uplink tunnel to `roko.pstch.net``
    tun-uplink = {
      netdevConfig = {
        Kind = "wireguard";
        Name = "tun-uplink";
      };
      wireguardConfig = {
        PrivateKeyFile = "/etc/keys/wireguard/tun-uplink/notix.priv";
        ListenPort = 54224;
      };
      wireguardPeers = [
        { wireguardPeerConfig = {
            Endpoint = "51.159.37.56:14224";
            AllowedIPs = [
              "0.0.0.0/0"     # allow using as default gateway
              "224.0.0.0/8"   # allow OSPF traffic
            ];
            PublicKey = "K2vuQGMyV3GgcXp/FJ2D8Rbv5o9HPrcgG6mUyLFlVh8=";
            PresharedKeyFile = "/etc/keys/wireguard/tun-uplink/shared.key";
            PersistentKeepalive = 5;
        }; }
      ];
    };

    # Downlink bridge to `tinix.lebib.org`
    br-tinix = {
      netdevConfig = {
        Kind = "bridge";
        Name = "br-tinix";
      };
      extraConfig = ''
        [Bridge]
        STP=off
      '';
    };

    # Downlink bridge to `bibix.lebib.org`
    br-bibix = {
      netdevConfig = {
        Kind = "bridge";
        Name = "br-bibix";
      };
      extraConfig = ''
        [Bridge]
        STP=off
      '';
    };
  };

  systemd.network.networks = {
    # Loopback interface
    loopback = {
      name = "lo";
      address = [ "127.0.0.1/8" "10.14.255.224/32" ];
    };

    # Uplink bridge (@despo)
    br-uplink-1 = {
      name = "br-uplink-1";
      addresses = [
        { addressConfig = { Address = "192.168.1.224/24"; }; }
      ];
      gateway = [ "192.168.1.254" ];
    };
    eno1 = { name = "eno1"; bridge = [ "br-uplink-1" ]; };
    eth1 = { name = "eth1"; bridge = [ "br-uplink-1" ]; };

    # Uplink bridge (@benbart)
    br-uplink-2 = {
      name = "br-uplink-2";
      addresses = [
        { addressConfig = { Address = "192.168.2.224/24"; }; }
      ];
      gateway = [ "192.168.2.254" ];
    };
    eno2 = { name = "eno2"; bridge = [ "br-uplink-2" ]; };
    eth0 = { name = "eth0"; bridge = [ "br-uplink-2" ]; };

    # Uplink tunnel to `roko.pstch.net`
    tun-uplink = {
      name = "tun-uplink";
      addresses = [
        { addressConfig = { Address = "10.14.255.224/32"; Peer = "10.14.255.255/32"; }; }
      ];
      routes = [
        { routeConfig = { Destination = "10.14.0.0/16" ; Scope = "link"; }; }
        { routeConfig = { Destination = "10.14.255.240/32" ; Scope = "link"; }; }
      ];
      linkConfig.RequiredForOnline = false;
    };

    # Downlink bridge to `tinix.lebib.org`
    br-tinix = {
      name = "br-tinix";
      addresses = [
        { addressConfig = { Address = "10.14.255.224/32"; Peer = "10.14.255.208/32"; }; }
      ];
      routes = [
        # Route to tinix containers
        { routeConfig = { Destination = "10.14.208.0/22"; Scope = "link"; }; }
      ];
      linkConfig.RequiredForOnline = false;
    };

    # Downlink bridge to `bibix.lebib.org`
    br-bibix = {
      name = "br-bibix";
      addresses = [
        { addressConfig = { Address = "10.14.255.224/32"; Peer = "10.13.255.208/32"; }; }
      ];
      routes = [
        { routeConfig = { Destination = "10.12.0.0/16"; Scope = "link"; }; }
        { routeConfig = { Destination = "10.13.0.0/16"; Scope = "link"; }; }
      ];
      linkConfig.RequiredForOnline = false;
    };
  };
}
